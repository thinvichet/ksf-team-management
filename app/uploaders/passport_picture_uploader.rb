class PassportPictureUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    'default-profile.png'
  end

  version :thumb do
    process resize_to_fill: [50, 50]
  end

  version :default do
    process resize_to_fill: [220, 290]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
