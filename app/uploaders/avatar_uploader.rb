class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::RMagick

  process :auto_orient

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    'default-profile.png'
  end

  version :large do
    resize_to_limit(600, 600)
  end

  version :thumb do
    process resize_to_fill: [50, 50]
  end

  version :main do
    process :crop
    resize_to_fill(400, 515)
  end

  version :default do
    process resize_to_fill: [220, 290]
  end

  def crop
    if model.crop_x.present?
      resize_to_limit(600, 600)
      manipulate! do |img|
        x = model.crop_x.to_i
        y = model.crop_y.to_i
        w = model.crop_w.to_i
        h = model.crop_h.to_i
        img.crop!(x, y, w, h)
      end
    end
  end

  def auto_orient
    manipulate! do |image|
      case image['EXIF:Orientation'].to_i
      when 2
        image.flop
      when 3
        image.rotate(180)
      when 4
        image.flip
      when 5
        image.transpose
      when 6
        image.rotate(90)
      when 7
        image.transverse
      when 8
        image.rotate(270)
      end

      image
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def timestamp
    var = :"@#{mounted_as}_timestamp"
    model.instance_variable_get(var) or model.instance_variable_set(var, Time.now.to_i)
  end
end
