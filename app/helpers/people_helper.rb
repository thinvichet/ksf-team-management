module PeopleHelper
  def profile_pic_label_color(person)
    return 'avatar-danger' if person.nil?
    person.profile_pic_expire? ? 'avatar-danger' : 'avatar-success'
  end

  def check_person_user_role
    return 'ប្រធានមន្ទីរអប់រំ​ យុវជន និងកីឡា' unless @person.user.role == 'sport_representer'
    @person.user.organization.try(:name) =~ /សហព័ន្ធ/ ? 'ប្រធានសហព័ន្ធ' : 'ប្រធាននាយកដ្ឋានអប់រំកាយ​ និងកីឡា'
  end

  def khmer_athlete_display(person)
    if person.card_type.present?
      person.card_type.name.downcase == 'athlete' ? person.sex.downcase.titleize : person.card_type.name
    else
      person.person_type.titleize
    end
  end
end
