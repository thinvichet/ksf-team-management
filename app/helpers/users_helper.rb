module UsersHelper
  def lock_button(lock_state)
    lock_state ? 'lock' : 'unlock'
  end
end
