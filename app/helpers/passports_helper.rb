module PassportsHelper
  def passport_label_color(state)
    case state
    when 'Pending'
      klass = 'label label-warning'
    when 'Verify'
      klass = 'label label-success'
    when 'Invalid'
      klass = 'label label-danger'
    when 'Expired'
      klass = 'label label-danger'
    else
      klass = 'label label-danger'
      state = 'No Passport'
    end
    content_tag(:span, state, class: klass)
  end
end
