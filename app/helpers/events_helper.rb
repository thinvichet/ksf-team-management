module EventsHelper
  def event_state_label(state)
    case state
    when 'Open'
      klass = 'label label-info'
    when 'Close'
      klass = 'label label-warning'
    when 'Start'
      klass = 'label label-success'
    when 'End'
      klass = 'label label-danger'
    end
    content_tag(:span, state, class: klass)
  end
end
