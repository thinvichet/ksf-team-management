module SportSubmissionsHelper
  def reorder_sport_category(first_sport_group, last_sport_group)
    continue_loop = true
    new_first_sport_group = first_sport_group
    while continue_loop do
      if first_sport_group.last.parent_id.present? && first_sport_group.last.parent_id == last_sport_group.first.parent_id
        a = last_sport_group.shift
        new_first_sport_group  << a
      else
        continue_loop = false
      end
    end
    [new_first_sport_group, last_sport_group]
  end

  def sport_category_male(sport, user_sport_category_ids)
    male_sport = sport.children.find_by(status: "Male")
    male_sport.present? && user_sport_category_ids.include?(male_sport.id)
  end

  def sport_category_female(sport, user_sport_category_ids)
    female_sport = sport.children.find_by(status: "Female")
    female_sport.present? && user_sport_category_ids.include?(female_sport.id)
  end
end
