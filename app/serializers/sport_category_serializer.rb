class SportCategorySerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :levels
end
