class PersonSerializer < ActiveModel::Serializer
  attributes :id, :name, :avatar, :avatar_thumb_path, :date_of_birth, :sex, :passport_status, :person_type

  def name
    object.sure_name_kh + ' ' + object.given_name_kh
  end

  def avatar_thumb_path
    object.avatar.thumb.url
  end

  def passport_status
    case object.try(:passport).try(:state)
    when 'Pending'
      klass = 'label label-warning'
    when 'Verify'
      klass = 'label label-success'
    when 'Invalid'
      klass = 'label label-danger'
    when 'Expired'
      klass = 'label label-danger'
    else
      klass = 'label label-danger'
      state = 'No Passport'
    end
    "<span class='#{klass}'>#{state}</span>"
  end

  def person_type
    if object.athlete?
      'Athletes'
    elsif object.referee?
      'Referees'
    elsif object.nf_delegate?
      'NF Delegate'
    elsif object.if_delegate?
      'IF Delegate'
    else
      'Other'
    end
  end

  belongs_to :sport_category

end
