class LevelSerializer < ActiveModel::Serializer
  attributes :id, :level_label_with_sport, :competition_name

  def level_label_with_sport
    "#{object.competition_name} #{object.measurement_type}"
  end
end
