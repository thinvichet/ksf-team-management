class Notifier

  @client = Twilio::REST::Client.new('AC1b502cee0df7f749ba4eaae020920acf', '5740e7510aeba22aae9edacc6cac8d2b')
  @send_from = '+15123374830'

  def initialize(send_to, body)
    @send_to = send_to
    @body = body
  end

  def call
    @client.messages.create(from: @send_from, to: @send_to, body: @body)
  end

  def self.alert_to(phones, body)
    phones.each do |phone|
      begin
        @client.messages.create(from: @send_from, to: phone, body: body)
      rescue
        print('could not send sms')
      end
    end
  end

end
