class SportRepresenterOrganizationPolicy < ApplicationPolicy
  def index?
    user.sport_representer?
  end

  def show?
    user.sport_representer?
  end
end
