class  AdminPolicy < ApplicationPolicy
  def index?
    admin_view?
  end

  def new?
    create?
  end

  def create?
    user.admin?
  end

  def show?
    admin_view?
  end

  def edit?
    update?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def team_member_card?
    admin_view?
  end
end
