class UserPolicy < ApplicationPolicy
  def index?
    admin_view?
  end

  def new?
    create?
  end

  def create?
    user.admin?
  end

  def show?
    admin_view? || record == user
  end

  def edit?
    update?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def disable?
    user.admin?
  end
end
