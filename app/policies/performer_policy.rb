class PerformerPolicy < AdminPolicy
  def index?
    admin_view? || user.performer_manager?
  end

  def new?
    create?
  end

  def create?
    user.admin? || user.performer_manager?
  end

  def show?
    admin_view? || user.performer_manager?
  end

  def edit?
    update?
  end

  def update?
    user.admin? || user.performer_manager?
  end

  def destroy?
    user.admin? || user.performer_manager?
  end

  def team_member_card?
    user.admin?
  end
end
