class SportSubmissionPolicy < ApplicationPolicy
  def edit?
    user.admin?
  end

  def update?
    edit?
  end

  def new?
    user.organization.sport_submissions.last.try(:game) != record.game
  end

  def create?
    new?
  end
end
