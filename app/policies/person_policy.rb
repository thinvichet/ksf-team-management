class PersonPolicy < AdminPolicy
  def index?
    admin_view? || user.representer? || user.sport_representer?
  end

  def new?
    create?
  end

  def create?
    # user.admin? || !user.representer? || !user.sport_representer?
    user.admin? || user.sport_representer?
  end

  def show?
    admin_view? || user.representer? || user.sport_representer?
  end

  def edit?
    update?
  end

  def update?
    user.admin? || user.representer? || user.sport_representer?
    # user.admin? || user.representer?
  end

  def destroy?
    user.admin? || user.representer? || user.sport_representer?
  end

  def check_availibilities?
    user.food_scanner? || user.accommodation_scanner? || user.activator? || user.admin?
  end

  def confirm?
    check_availibilities?
  end
end
