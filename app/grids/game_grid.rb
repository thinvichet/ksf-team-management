class GameGrid
  include Datagrid

  scope { Game.all }

  column :name, header: I18n.t('admin.games.index.name')

  column :sports_count, header: I18n.t('admin.games.index.sports_count')  do |game|
    game.sport_categories.length
  end

  column :sport_categories, header: I18n.t('admin.games.index.sport') do |game|
    game.sport_categories.map(&:name).join(', ')
  end
  column :action, header: I18n.t('admin.games.index.action'), html: true do |game|
    render 'admin/games/action', game: game
  end
end
