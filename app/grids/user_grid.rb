class UserGrid
  include Datagrid

  scope { User.all }

  filter :sure_name_kh, :string, header: I18n.t('user_attr.sure_name_kh') do |value|
    where("sure_name_kh ILIKE ?", "%#{value}%")
  end
  filter :given_name_kh, :string, header: I18n.t('user_attr.given_name_kh') do |value|
    where("given_name_kh ILIKE ?", "%#{value}%")
  end
  filter :sure_name_en, :string, header: I18n.t('user_attr.sure_name_en') do |value|
    where("sure_name_en ILIKE ?", "%#{value}%")
  end
  filter :given_name_en, :string, header: I18n.t('user_attr.given_name_en') do |value|
    where("given_name_en ILIKE ?", "%#{value}%")
  end
  filter :email, :string, header: I18n.t('user_attr.email') do |value|
    where("email ILIKE ?", "%#{value}%")
  end
  filter :phone, :string, header: I18n.t('user_attr.phone') do |value|
    where("phone ILIKE ?", "%#{value}%")
  end
  filter :title, :string, header: I18n.t('user_attr.title') do |value|
    where("title ILIKE ?", "%#{value}%")
  end
  filter :role, :enum, select: User::ROLES, prompt: 'Select Sport', class: 'select2', header: I18n.t('user_attr.role') do |value|
    where("role ILIKE ?", "%#{value}%")
  end
  filter :sport_category_id, :enum, select: SportCategory.pluck(:name, :id), prompt: 'Select Sport', class: 'select2', header: I18n.t('user_attr.sport_category')
  filter :organization_id, :enum, select: Organization.pluck(:name, :id), prompt: 'Select Organization', class: 'select2', header: I18n.t('user_attr.organization')

  column :id, html: true, header: I18n.t('user_attr.id') do |user|
    link_to user.id, user_path(user)
  end
  column :email, html: true, header: I18n.t('user_attr.email') do |user|
    link_to user.email, user_path(user)
  end
  column :role, html: true, header: I18n.t('user_attr.role') do |user|
    link_to user.role, user_path(user)
  end
  column :action, html: true, header: I18n.t('actions.action') do |user|
    render 'users/action', user: user
  end
end
