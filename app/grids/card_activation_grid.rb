class CardActivationGrid
  include Datagrid

  scope do
    Person.joins(:card_activations).where('card_activations.game_id': Game.current_active.id).distinct
  end

  column 'Profile Picture', html: true, order: false do |person|
    image_tag(person.avatar_url(:thumb), class: "image-responsive person-avatar #{profile_pic_label_color(person)}")
  end

  column :name, html: true, header: I18n.t('person_attr.name')  do |person|
    link_to person.name, person_path(person)
  end

  column :sex, header: I18n.t('person_attr.gender')

  column :sport, html: true, header: I18n.t('person_attr.sport') do |obj|
    obj.sport_category_name.try(:titleize)
  end
end
