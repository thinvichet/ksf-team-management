class CardTypeGrid
  include Datagrid

  scope { CardType.order(:letter) }

  column :name, header: I18n.t('admin.card_types.index.name')
  column :letter, header: I18n.t('admin.card_types.index.letter'), class: 'text-center'
  column :allow_food, header: I18n.t('admin.card_types.index.allow_food'), class: 'text-center', html: true do |card_type|
    if card_type.allow_food
      content_tag(:i, nil, class: 'fa fa-check icon-success')
    else
      content_tag(:i, nil, class: 'fa fa-times icon-danger')
    end
  end

  column :action, header: I18n.t('admin.card_types.index.action'), html: true do |card_type|
    render 'admin/card_types/action', card_type: card_type
  end
end
