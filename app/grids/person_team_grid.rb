class PersonTeamGrid
  include Datagrid
  attr_accessor :current_user

  scope { Person.all }

  filter :sure_name_kh, :string, header: I18n.t('person_attr.sure_name_kh') do |value|
    where("sure_name_kh ILIKE ?", "%#{value}%")
  end
  filter :given_name_kh, :string, header: I18n.t('person_attr.given_name_kh') do |value|
    where("given_name_kh ILIKE ?", "%#{value}%")
  end
  filter :sex, :enum, select: ['Male', 'Female'], header: I18n.t('person_attr.gender'), class: 'select2'
  filter :email, :string, header: I18n.t('person_attr.email') do |value|
    where("email ILIKE ?", "%#{value}%")
  end
  filter :date_of_birth, :date, header: I18n.t('person_attr.dob')
  filter :person_type, :enum, select: PersonTeam::POSITION, prompt: 'Select Position', class: 'select2', header: I18n.t('person_attr.position') do |value|
    where('position = ?', value)
  end
  filter :team, :enum, select: :team_by_user, prompt: 'Select Team', class: 'select2', header: I18n.t('person_attr.team') do |value|
    joins(:teams).where('teams.id = ?', value)
  end
  filter :sport_category_id, :enum, select: SportCategory.sport_submission_collection.pluck(:name, :id), prompt: 'Select Sport', class: 'select2', header: I18n.t('person_attr.sport')

  filter :organization_id, :enum, select: Organization.pluck(:name, :id), prompt: 'Select Organization', class: 'select2', header: I18n.t('person_attr.organization')

  column 'Profile Picture', html: true, order: false do |person|
    image_tag(person.avatar_url(:thumb), class: "image-responsive person-avatar #{profile_pic_label_color(person)}")
  end

  column :name, html: true, header: I18n.t('person_attr.name')  do |person|
    link_to person.name, person_path(person)
  end

  column 'Passport Status', html: true, header: I18n.t('person_attr.passport_status') do |person|
    passport_label_color(person.passport.try(:state))
  end
  column :sex, header: I18n.t('person_attr.gender')
  column :date_of_birth, header: I18n.t('person_attr.dob')

  column :sport, html: true, header: I18n.t('person_attr.sport') do |obj|
    obj.sport_category_name.try(:titleize)
  end

  column :type, html: true, header: I18n.t('person_attr.person_type') do |obj|
    obj.position
  end

  def team_by_user
    if current_user
      Team.where(user: current_user).pluck(:name, :id)
    else
      Team.pluck(:name, :id)
    end
  end
end
