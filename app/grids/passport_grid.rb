class PassportGrid
  include Datagrid

  scope { Passport.joins(:person) }

  filter :person_name, :string, header: I18n.t('admin.passports.index.person_name') do |value|
    where('people.sure_name_kh ILIKE :name OR people.given_name_kh ILIKE :name', name: "%#{value}%")
  end

  filter :state, :enum, select: Passport::STATE, class: 'select2', header: I18n.t('admin.passports.index.state') do |value|
    where(state: value)
  end

  column :name, header: I18n.t('admin.passports.index.person_name')
  column :state, header: I18n.t('admin.passports.index.state'), html: true do |passport|
    passport_label_color(passport.state)
  end
  column :passport_validity, header: I18n.t('admin.passports.index.passport_validity')
  column :nationality, header: I18n.t('admin.passports.index.nationality')
  column :passport_number, header: I18n.t('admin.passports.index.passport_number')

  column :action, header: I18n.t('actions.action'), html: true do |passport|
    render 'admin/passports/action', passport: passport
  end
end
