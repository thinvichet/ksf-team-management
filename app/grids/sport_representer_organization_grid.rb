class SportRepresenterOrganizationGrid
  include Datagrid

  scope do
    Organization.non_admin
  end

  filter(:short_code, header: I18n.t('admin.organizations.index.short_code')) do |value|
    where('short_code ilike ?', "%#{value}%")
  end

  filter(:name, header: I18n.t('admin.organizations.index.name'))
  filter(:org_label, header: I18n.t('admin.organizations.index.org_label'))

  column(:short_code, header: I18n.t('admin.organizations.index.short_code'))
  column(:name, header: I18n.t('admin.organizations.index.name'))
  column(:org_label, header: I18n.t('admin.organizations.index.org_label'))

  column(:user, header: I18n.t('admin.organizations.index.member_count')) do |model|
    model.users.size
  end

  def to_spreadsheet(sport_ids)
    xlsx_package = Axlsx::Package.new
    wb = xlsx_package.workbook

    to_header = wb.styles.add_style sz: 17,
                                    font_name: 'Khmer Moul',
                                    alignment: {  horizontal: :center,
                                      vertical: :center,
                                      wrap_text: true,
                                      shrink_to_fit: true
                                    }
    highlight_cell_header = wb.styles.add_style fg_color: "FFFFFF",
                              b: true,
                              bg_color: "004586",
                              sz: 12,
                              shrink_to_fit: true,
                              border: { style: :thin, color: "00" },
                              alignment: {  vertical: :center,
                                            horizontal: :center,
                                            shrink_to_fit: true
                                          }
    highlight_cell_data = wb.styles.add_style b: false,
                                              sz: 12,
                                              font_name: 'Khmer OS Content',
                                              border: { style: :thin, color: "00" },
                                              alignment: { horizontal: :center,
                                                          shrink_to_fit: true
                                                        }
    wb.styles.fonts.first.name = 'Khmer Moul'
    date_cell = wb.styles.add_style(format_code: "d-mmm-yy", border: Axlsx::STYLE_THIN_BORDER)
    left_algn_cell = wb.styles.add_style b: false,
                                              sz: 12,
                                              font_name: 'Khmer OS Content',
                                              border: { style: :thin, color: "00" },
                                              alignment: { horizontal: :left,
                                                          shrink_to_fit: true
                                                        }
    total_header = wb.styles.add_style fg_color: "FFFFFF",
                              b: true,
                              bg_color: "fA3E21",
                              sz: 12,
                              shrink_to_fit: true,
                              border: { style: :thin, color: "00" },
                              alignment: {  horizontal: :center,
                                            shrink_to_fit: true
                                          }

    organizations = Organization.only_organization_reprenter.non_admin.order('name')

    wb.add_worksheet(name: "sport_submission") do |sheet|
      sheet.add_row ['ការដកស្រង់ការចុះឈ្មោះរបស់ក្រសួង រាជធានី ខេត្ត'], style: to_header

      sheet.add_row ['លរ', 'ប្រភេទកីឡា/ក្រសួង រាជធានី ខេត្ត', organizations.pluck(:name), 'សរុប'].flatten,
             style: highlight_cell_header

      parent_has_child_ids = SportCategory.joins(:children).ids.uniq
      sport_categories = SportCategory.where(id: sport_ids).where.not(id: parent_has_child_ids).order(:parent_id)

      sport_categories.each_with_index do |sport, index|
        organization_ids = sport.organizations.ids
        sheet.add_row [
                        index + 1,
                        sport.name,
                        organizations.map{ |org| organization_ids.include?(org.id) ? '✔' : '' },
                        organization_ids.count
                      ].flatten, style: highlight_cell_data

      end
      sheet.add_row([
        '',
        'សរុប',
        organizations.map{ |org| org.sport_submissions.where(game: Game.current_active).joins(:sport_categories).where(sport_categories: {id: sport_ids}).size },
        organizations.sum{ |org| org.sport_submissions.where(game: Game.current_active).joins(:sport_categories).where(sport_categories: {id: sport_ids}).size },
      ].flatten, style: highlight_cell_data)

      autofit(sheet)
      col_widths = [5]

      sheet.rows.first.height = 40
      sheet.merge_cells("A1:C1")
      sheet
      sheet["B2:AE2"].each do |c|
        text_length = (c.value.length < 5) ? c.value.length * 2 : c.value.length
        col_widths << text_length
        c.font_name = "Khmer OS Content"
        c.color  = "FFFFFF"
        c.sz = 14
      end

      sheet.column_widths *col_widths

      sheet["B3:B#{sheet.rows.length}"].each do |c|
        c.font_name = "Khmer OS Content"
        c.b = true
        c.style = left_algn_cell
        c.sz = 12
      end

      sheet["AE3:AE#{sheet.rows.length}"].each do |c|
        next if c.value.to_i >= 5
        c.style = total_header
      end
    end

    buffer = StringIO.new
    xlsx_package.use_shared_strings = true
    buffer.write(xlsx_package.to_stream.read)
    buffer.string
  end

  private

  def autofit(worksheet)
    (0...worksheet.column_info.count).each do |col|
      @high = 1
      row = 0
      worksheet.cells.each do |cell|
        w = cell==nil || cell=='' ? 1 : cell.to_s.strip.split('').count+2
        w = (w*1).round
        if w > @high
          w >= 50 ? @high = 50 : @high = w
        end
        row = row+1
      end
      worksheet.column_info[col].width = @high
    end
    (0...worksheet.rows.count).each do |row|
      @high = 1
      col = 0
      worksheet.rows[row].each do |cell|
        next if cell.nil?
        w = 14
        if w >= @high
          cell.send(:autowidth) > 52 ? @high = w * 2 : @high = w
        end
        col = col+1
      end
      current_row = worksheet.rows[row]
      current_row.height = @height
    end
  end
end
