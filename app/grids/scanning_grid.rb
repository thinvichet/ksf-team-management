class ScanningGrid
  include Datagrid

  scope do
    Scanning
      .select(
        :user_id,
        :person_id,
        'count(*) as scanning_count',
        'max(created_at) as created_at',
        'bool_and(succeed) as succeed',
        'max(message) as message')
      .where(game_id: Game.current_active)
      .group(:user_id, :person_id)
      .order('max(created_at) desc')
  end

  filter :person_id, :enum, select: :people_collection, prompt: 'Select Person', class: 'select2', header: I18n.t('scannings.index.person')
  filter :user_id, :enum, select: :scanner_collection, prompt: 'Select Person', class: 'select2', header: I18n.t('scannings.index.scanned_by')
  filter :created_at, :date, range: true, header: 'Scanned date', header: I18n.t('scannings.index.scanned_date')

  column 'Profile Picture', html: true, header: I18n.t('scannings.index.profile_picture'), order: false do |model|
    image_tag(model.person.avatar_url(:thumb), class: "image-responsive person-avatar")
  end
  column(:person, header: I18n.t('scannings.index.person'), &:person_name)
  column(:scanned_by, header: I18n.t('scannings.index.scanned_by'), &:user_fullname_kh)
  column(:last_scan_date, header: I18n.t('scannings.index.last_scan_date')) do |model|
    model.created_at.strftime("%d-%b-%Y")
  end
  column(:scanning_count, header: I18n.t('scannings.index.scanning_count'))
  column(:succeed, html: true, header: I18n.t('scannings.index.succeed')) do |model|
    if model.succeed
      content_tag(:i, nil, class: 'fa fa-check icon-success')
    else
      content_tag(:i, nil, class: 'fa fa-times icon-danger')
    end
  end

  column(:message, header: I18n.t('scannings.index.reason'))

  private

  def scanner_collection
    User.where(role: ['food_scanner', 'accommodation_scanner']).map{ |user| [user.fullname_kh, user.id] }
  end

  def people_collection
    Person.all.map{ |person| [person.name, person.id] }
  end
end
