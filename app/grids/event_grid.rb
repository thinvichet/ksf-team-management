class EventGrid
  include Datagrid

  scope { Event.all }

  column :game, html: true do |event|
    event.game.name
  end

  column :level, header: I18n.t('admin.events.index.level_name'), html: true do |event|
    event.level.competition_name
  end

  column :state, header: I18n.t('admin.events.index.state'), html: true do |event|
    content_tag :span, event.state, class: 'label label-info'
  end
  column :started_at, header: I18n.t('admin.events.index.start_date')
  column :ended_at, header: I18n.t('admin.events.index.end_date')
  column :open_registered_at, header: I18n.t('admin.events.index.open_registered_at')
  column :close_registered_at, header: I18n.t('admin.events.index.close_registered_at')
  column :location, header: I18n.t('admin.events.index.location')

  column :action, header: I18n.t('actions.action'), html: true do |event|
    render 'admin/events/action', event: event
  end
end
