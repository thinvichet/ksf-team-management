class OrganizationsGrid
  include Datagrid

  scope do
    Organization
  end

  filter(:short_code, header: I18n.t('admin.organizations.index.short_code')) do |value|
    where('short_code ilike ?', "%#{value}%")
  end

  filter(:name, header: I18n.t('admin.organizations.index.name'))
  filter(:org_label, header: I18n.t('admin.organizations.index.org_label'))

  column 'Logo', html: true, order: false do |org|
    image_tag(org.logo_url(:thumb), class: "image-responsive person-avatar #{edit_admin_organization_path(org)}")
  end
  column(:short_code, header: I18n.t('admin.organizations.index.short_code'))
  column(:name, header: I18n.t('admin.organizations.index.name'))
  column(:org_label, header: I18n.t('admin.organizations.index.org_label'))

  column(:user, header: I18n.t('admin.organizations.index.member_count')) do |model|
    model.users.size
  end

  column(:created_at) do |model|
    model.created_at.to_date
  end

  column :action, header: I18n.t('actions.action'), html: true do |model|
    render 'admin/organizations/action', model: model
  end
end
