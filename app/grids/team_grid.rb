class TeamGrid
  include Datagrid

  scope { Team.all }

  column :name
  column :status, html: true do |team|
    content_tag(:span, 'Joint', class: 'label label-success')
  end
  column :representer, html: true do |team|
    team.user.username
  end
  column :action, html: true do |team|
    render 'teams/action', team: team
  end
end
