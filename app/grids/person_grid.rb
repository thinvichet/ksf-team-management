class PersonGrid
  include Datagrid
  attr_accessor :current_user

  scope { Person.includes(:passport, :sport_category, :user, :card_type).order(updated_at: :desc) }

  filter :sure_name_kh, :string, header: I18n.t('person_attr.sure_name_kh') do |value|
    where("sure_name_kh ILIKE ?", "%#{value}%")
  end
  filter :given_name_kh, :string, header: I18n.t('person_attr.given_name_kh') do |value|
    where("given_name_kh ILIKE ?", "%#{value}%")
  end
  filter :sex, :enum, select: ['Male', 'Female'], header: I18n.t('person_attr.gender'), class: 'select2'
  filter :email, :string, header: I18n.t('person_attr.email') do |value|
    where("email ILIKE ?", "%#{value}%")
  end
  filter :weight, :integer, header: I18n.t('person_attr.weight') do |value|
    where("weight = ?", value)
  end
  filter :date_of_birth, :date, header: I18n.t('person_attr.dob')
  filter :person_type, :enum, select: Person::PERSON_TYPE, prompt: 'Select Person Type', class: 'select2', header: I18n.t('person_attr.person_type')
  filter :card_type, :enum, select: CardType.pluck(:name, :id), prompt: '---មុខសញ្ញាផ្តល់ជូន---', class: 'select2', header: I18n.t('person_attr.given_role')
  filter :team, :enum, select: :team_by_user, prompt: 'Select Team', class: 'select2', header: I18n.t('person_attr.team') do |value|
    joins(:teams).where('teams.id = ?', value)
  end
  filter :sport_category_id, :enum, select: SportCategory.all.distinct.pluck(:name, :id), prompt: 'Select Sport', class: 'select2', header: I18n.t('person_attr.sport') do |value|
    person_sub_sport_categories = joins(:person_sub_sport_categories).where(person_sub_sport_categories: {sport_category_id: value}).distinct
    person_sub_sport_categories.present? ? person_sub_sport_categories : where('people.sport_category_id': value)
  end

  filter :event, :enum, select: :people_events, prompt: '--សូមជ្រើសរើសប្រភេទវិញ្ញាសារ--', class: 'select2', header: I18n.t('person_attr.event') do |value|
    # joins(:events).where('events.id = ?', value)
    sport_name, competition_name = value.split(':') || ['', '']
    joins(events: [game_sport_category_level: [:level, game_sport_category: :sport_category]]).where("sport_categories.name = ? AND levels.competition_name = ?", sport_name, competition_name.squish)
  end

  filter :organization_id, :enum, select: Organization.pluck(:name, :id), prompt: 'Select country', class: 'select2', header: I18n.t('person_attr.organization')

  column 'Profile Picture', html: true, order: false do |person|
    image_tag(person.avatar_url(:thumb), class: "image-responsive person-avatar #{profile_pic_label_color(person)}")
  end

  column :name, html: true, header: I18n.t('person_attr.name')  do |person|
    link_to person.name_en, person_path(person)
  end

  column :activation, header: I18n.t('people.index.card_activation'), html: true do |person|
    if person.card_activations.of_current_game.try(:is_active?)
      content_tag(:i, nil, class: 'fa fa-check icon-success')
    else
      content_tag(:i, nil, class: 'fa fa-times icon-danger')
    end
  end

  column :sex, header: I18n.t('person_attr.gender')
  column :date_of_birth, header: I18n.t('person_attr.dob')

  column :organization, html: true, header: 'Country' do |obj|
    obj.organization.try(:name)
  end

  # column :event, html: true, header: I18n.t('person_attr.sport') do |obj|
  #   obj.teams.pluck(:name).join(', ')
  # end

  column :type, html: true, header: I18n.t('person_attr.person_type') do |obj|
    obj.card_type.present? ? obj.card_type.name : obj.person_type.titleize
  end

  column :action, html: true, header: I18n.t('person_attr.action') do |person|
    render 'people/action', person: person
  end

  def team_by_user
    if current_user
      Team.where(user: current_user).pluck(:name, :id)
    else
      Team.pluck(:name, :id)
    end
  end

  def people_events
    game = Game.current_active
    return [] if game.blank?
    game.events.eager_load(game_sport_category_level: [level: :sport_category]).map do |event|
      [event.level_label_with_sport]
    end
  end

  def to_spreadsheet
    xlsx_package = Axlsx::Package.new
    wb = xlsx_package.workbook
    highlight_cell_header = wb.styles.add_style fg_color: "FFFFFF",
                              b: true,
                              bg_color: "004586",
                              sz: 12,
                              border: { style: :thin, color: "00" },
                              alignment: { horizontal: :center,
                                              vertical: :center ,
                                              wrap_text: true}
    highlight_cell_data = wb.styles.add_style b: false,
                                              sz: 12,
                                              font_name: 'Khmer OS Content',
                                              border: { style: :thin, color: "00" },
                                              alignment: { horizontal: :center,
                                                          vertical: :center ,
                                                          wrap_text: true
                                                        }
    wb.styles.fonts.first.name = 'Khmer OS Content'
    date_cell = wb.styles.add_style(format_code: "d-mmm-yy", border: Axlsx::STYLE_THIN_BORDER)
    wb.add_worksheet(name: "តារាងកីឡាករ") do |sheet|
      sheet.add_row ['កីឡាករ']
      sheet.add_row ['លរ', 'ឈ្មោះអក្សរខ្មែរ', 'ឈ្មោះអក្សរឡាតាំង', 'ភេទ', 'ថ្ងៃខែឆ្នាំកំណើត', 'អត្តសញ្ញាណប័ណ្ណ', 'សញ្ជាតិ', 'លអ', 'ប្រភេទកីឡា'],
             style: highlight_cell_header
      assets.except(:limit).each_with_index do |person, index|
        sheet.add_row [
                        index + 1,
                        person.name,
                        person.name_en.try(:upcase),
                        person.sex,
                        person.date_of_birth.strftime("%d-%b-%Y"),
                        person.identification_number,
                        person.citizenship,
                        person.uniform_number,
                        person.sport_category.try(:name)
                      ], style: highlight_cell_data
      end
      sheet["B2:I2"].each do |c|
        c.font_name = "Khmer OS Content"
        c.color  = "FFFFFF"
      end
      sheet["H3:H#{sheet.rows.length}"].each do |c|
        c.b         = true
        c.font_name = "Impact"
      end
    end

    buffer = StringIO.new
    xlsx_package.use_shared_strings = true
    buffer.write(xlsx_package.to_stream.read)
    buffer.string
  end
end
