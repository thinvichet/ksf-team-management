class PerformerGrid
  include Datagrid

  scope { Performer.all }

  column :name, html: true do |performer|
    link_to performer.name, performer_path(performer)
  end
  column :sex, header: I18n.t('person_attr.gender')
  column :date_of_birth, header: I18n.t('person_attr.dob')

  column :action, html: true, header: I18n.t('person_attr.action') do |performer|
    render 'performers/action', performer: performer
  end
end
