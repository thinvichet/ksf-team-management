TeamManagement.Initializer =
  exec: (pageName) ->
    if pageName && TeamManagement[pageName]
      TeamManagement[pageName]['init']()

  currentPage: ->
    return '' unless $('body').attr('id')

    bodyId      = $('body').attr('id').split('-')
    action      = @capitalize(bodyId[1])
    controller  = @capitalize(bodyId[0])
    controller + action

  init: ->
    TeamManagement.Initializer.exec('Common')
    if @currentPage()
      TeamManagement.Initializer.exec(@currentPage())


  capitalize: (value) ->
    value.replace /(^|\s)([a-z])/g, (m, p1, p2) ->
      p1 + p2.toUpperCase()

$(document).on 'ready page:load', ->
  TeamManagement.Initializer.init()
