TeamManagement.PeopleNew = TeamManagement.PeopleCreate = TeamManagement.PeopleEdit = TeamManagement.PeopleUpdate =
  init: ->
    @_handleAddAvatar()
    @_initAvatarCrop()
    @_initDateOfBirthPicker()

  _initDateOfBirthPicker: ->
    $('.datepicker').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      startView: 'years'
      autoclose: true
      disableTouchKeyboard: true
      defaultViewDate:
        year: '1990'
    )

  _handleAddAvatar: ->
    self = @
    uploader = $('#avatar').find('input[type=file]')
    placeholder = $('.avatar-thumb')
    
    placeholder.previewImage
      uploader: uploader

  _initAvatarCrop: ->
    $('#avatar-cropbox').Jcrop
      aspectRatio: 35 / 45
      setSelect: [0, 0, 400, 600]
      onSelect: @update
      onChange: @update


  update: (coords) ->
    # @updatePreview(coords)
    $('#person_crop_x').val(coords.x)
    $('#person_crop_y').val(coords.y)
    $('#person_crop_w').val(coords.w)
    $('#person_crop_h').val(coords.h)

  updatePreview: (coords) ->
    $('#preview').css
      width: Math.round(100/coords.w * $('#cropper').width()) + 'px'
      height: Math.round(100/coords.w * $('#cropper').height()) + 'px'
      marginLeft: '-' + Math.round(100/coords.w * coords.x) + 'px'
      marginLeft: '-' + Math.round(100/coords.h * coords.y) + 'px'
