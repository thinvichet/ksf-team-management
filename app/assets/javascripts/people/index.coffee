TeamManagement.PeopleIndex =
  init: ->
    @_initDateOfBirthPicker()
    @_removeCardActivation()

  _initDateOfBirthPicker: ->
    $('.datepicker.date_of_birth').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      startView: 'years'
      autoclose: true
      disableTouchKeyboard: true
      defaultViewDate:
        year: '1990'
    )

  _removeCardActivation: ->
    if $('.user-info').data('role') != 'admin'
      $('th.activation, td.activation').remove()
