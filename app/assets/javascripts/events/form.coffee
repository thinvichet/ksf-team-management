TeamManagement.EventsNew = TeamManagement.EventsCreate = TeamManagement.EventsEdit = TeamManagement.EventsUpdate =
  init: ->
    @_initDatePicker()

  _initDatePicker: ->
    $('.datepicker').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      autoclose: true
      disableTouchKeyboard: true
    )
