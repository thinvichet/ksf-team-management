#= require gentelella
#= require gentelella-custom
#= require cocoon
#= require bootstrap-datepicker
#= require select2
#= require unobtrusive_flash
#= require pnotify
#= require bootstrap-tagsinput.min.js
#= require jquery.Jcrop.min.js
#= require jquery.countdown.min

#= require namespace
#= require initializer
#= require common

#= require flash
#= require image_preview

#= require admin/games/form
#= require admin/games/show
#= require admin/game_sport_categories/form
#= require admin/sport_categories/form

#= require performers/index
#= require performers/form
#= require people/index
#= require people/form
#= require teams/form
#= require events/form
#= require passports/form

#= require scannings/index
#= require sport_submissions
#= require sport_submission_show
