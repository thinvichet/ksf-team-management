TeamManagement.GamesShow =
  init: ->
    @_initDateOfBirthPicker()

  _initDateOfBirthPicker: ->
    $('#person_team_grid_date_of_birth').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      startView: 'years'
      autoclose: true
      disableTouchKeyboard: true
      defaultViewDate:
        year: '1990'
    )
