TeamManagement.Game_sport_categoriesForm = TeamManagement.Game_sport_categoriesNew = TeamManagement.Game_sport_categoriesCreate = TeamManagement.Game_sport_categoriesEdit = TeamManagement.Game_sport_categoriesUpdate =
  init: ->
    @_handleSelectSport()

  _handleSelectSport: ->
    self = @
    $('.select-sport').on 'select2:select', (e) ->
      sportCategoryId = $(@).val()
      selectSportElement = @
      self._ajaxFillLevelBySport(sportCategoryId)

  _ajaxFillLevelBySport:(sportCategoryId) ->
    $.ajax
      method: 'GET'
      data:
        sport_category_id: sportCategoryId
      url: '/api/sport_categories/search'
      success: (response) ->
        select2Data = []
        levels = response.levels

        for level in levels
          select2Data.push { id: level.id, text: level.competition_name }

        $('.select-level').empty().select2({
          data: select2Data
        })
