TeamManagement.Sport_categoriesNew = TeamManagement.Sport_categoriesCreate = TeamManagement.Sport_categoriesEdit = TeamManagement.Sport_categoriesUpdate =
  init: ->
    @_initBootstrapInput()

  _initBootstrapInput: ->
    $('#sport_category_uniform_labels').tagsinput(
      confirmKeys: [13, 188]
    )

    $('.bootstrap-tagsinput input').on 'keypress', (e) ->
      if e.keyCode == 13
        e.keyCode = 188
        e.preventDefault()
