TeamManagement.PassportsNew = TeamManagement.PassportsCreate = TeamManagement.PassportsEdit = TeamManagement.PassportsUpdate =
  init: ->
    @_initDatePicker()
    @_handleAddPassportPicture()

  _initDatePicker: ->
    $('.datepicker').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      autoclose: true
      disableTouchKeyboard: true
    )

  _initDateOfBirthPicker: ->
    $('.datepicker').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      startView: 'years'
      autoclose: true
      disableTouchKeyboard: true
      defaultViewDate:
        year: '1990'
    )

  _handleAddPassportPicture: ->
    self = @
    uploader = $(@).closest('#passport-picture').find('input[type=file]')
    placeholder = $('.passport-picture')

    placeholder.previewImage
      uploader: uploader
