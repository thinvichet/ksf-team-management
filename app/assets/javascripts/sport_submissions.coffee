TeamManagement.Sport_submissionsNew = TeamManagement.Sport_submissionsCreate = TeamManagement.Sport_submissionsEdit = TeamManagement.Sport_submissionsUpdate =
  init: ->
    @_validateSportTypes()
    @_checkboxChecked()
    @_countDownTime()

  _validateSportTypes: ->
    self = @
    if $('#sport_submission_validation').val().length > 0
      self._handleError()

  _checkboxChecked: ->
    self = @
    $('input[type="checkbox"].styled').change (e) ->
      if $(this).prop('checked') == true
        $('small.help-block').remove()
        $('.check_boxes.sport_submission_sport_categories').removeClass 'has-error'
        e.stopPropagation()
      else
        self._handleError()
      return

  _handleError: ->
    sportTypeCheckedBox = $('.check_boxes.sport_submission_sport_categories')
    if $('input[type="checkbox"].styled:checked').length >= 1
      sportTypeCheckedBox.removeClass 'has-error'
    else
      sportTypeCheckedBox.addClass 'has-error'
      sportTypeCheckedBox.append '<small class="help-block" style="">សូមជ្រើសរើសប្រភេទកីឡាយ៉ាងហោចណាស់ឲបានមួយ។</small>'
      event.preventDefault()
      return

  _countDownTime: ->
    $('#clock').countdown('2018/02/25 12:34:56').on('update.countdown', (event) ->
      format = 'នៅសល់ %D ថ្ងៃ %H:%M:%S'
      $(this).html event.strftime(format)
      return
    ).on 'finish.countdown', (event) ->
      $(this).html('ការជ្រើសរើសប្រភេទកីឡាត្រូវបានបិទ!!!').parent().addClass 'disabled'
      return
