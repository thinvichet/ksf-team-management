TeamManagement.TeamsNew = TeamManagement.TeamsCreate= TeamManagement.TeamsEdit = TeamManagement.TeamsUpdate =
  init: ->
    @_handleSelectPersonChange()
    @_addAthleteCallback()
    @_handleAddAvatar()
    @_handleAfterAddPerson()

  _handleSelectPersonChange: ->
    self = @
    $('.person-select').on 'change', (e) ->
      self._handleSelectChange(@)

  _handleAfterAddPerson: ->
    self = @
    $('#person_teams').on 'cocoon:after-insert', (e, insertedItem) ->
      $('.select2').select2()
      personSelect = $(insertedItem).find('.person-select')
      self._handleSelectChange(personSelect)

  _handleSelectChange:(personSelect) ->
    self = @
    $(personSelect).on 'change', (e) ->
      personId = $(@).val()
      personRow = $(personSelect).closest('.person-row')

      self._ajaxFindPerson(personRow, personId)

  _fillPersonInformation:(container, data) ->
    $(container).find('.person-sex-col').empty()
    $(container).find('.person-dob-col').empty()
    $(container).find('.person-sport-col').empty()
    $(container).find('.person-passport-status-col').empty()
    avatar = data.avatar_thumb_path
    $(container).find('.pic').attr('src', avatar) if avatar != 'default-profile.png'
    $(container).find('.person-sex-col').append(data.sex)
    $(container).find('.person-dob-col').append(data.date_of_birth)
    $(container).find('.person-passport-status-col').append(data.passport_status)

    if data.person_type != 'Other'
      $(container).find('select.position-select').val(data.person_type).trigger('change')

    $(container).find('.person-sport-col').append(data.sport_category.name)
  _ajaxFindPerson:(personSelect, personId) ->
    self = @
    $.ajax
      url: '/api/people/find'
      data:
        person_id: personId
      dataType: 'JSON'
      success: (response) ->
        self._fillPersonInformation(personSelect, response)

  _AjaxGetPeople:(eventId, sportCategoryId) ->
    $.ajax
      url: '/api/people'
      data:
        event_id: eventId
        sport_category_id: sportCategoryId
      dataType: 'JSON'
      success: (response) ->
        selectedPeopleIds = $('#selected-people').data('selected-people')
        $('#team_person_ids option').remove()
        for person in response
          $('#team_person_ids').append("<option value=#{person.id}>#{person.name}</option>");
        if selectedPeopleIds.length > 0
          $('#team_person_ids').val(selectedPeopleIds)

  _addAthleteCallback: ->
    self = @
    $('#athletes').on 'cocoon:after-insert', (e, added_task) ->
      self._handleAddAvatar()
      $('.datepicker').datepicker(
        format: 'yyyy-mm-dd'
        todayHighlight: true
        startView: 'years'
        autoclose: true
        disableTouchKeyboard: true
        defaultViewDate:
          year: '1990'
      )

  _handleAddAvatar: ->
    self = @

    uploader = $(@).parent().find('input[type=file]')
    placeholder = $('.avatar')
    
    placeholder.previewImage
      uploader: uploader
