// flashes.js
$(document).ready(function() {
  $(window).bind('rails:flash', function(e, params) {
    new PNotify({
      // title: params.type,
      text: params.message,
      type: params.type,
      styling: "brighttheme",
      delay: 3000
    });
  });

  PNotify.prototype.options.delay ? (function() {
      PNotify.prototype.options.delay -= 500;
  }()) : (alert('Timer is already at zero.'))
});
