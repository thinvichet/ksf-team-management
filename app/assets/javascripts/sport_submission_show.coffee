TeamManagement.Sport_submissionsShow  =
  init: ->
    @_countDownTime()

  _countDownTime: ->
    $('#clock').countdown('2018/02/25 12:34:56').on('update.countdown', (event) ->
      format = 'នៅសល់ %D ថ្ងៃ %H:%M:%S'
      $(this).html event.strftime(format)
      return
    ).on 'finish.countdown', (event) ->
      $(this).html('ការជ្រើសរើសប្រភេទកីឡាត្រូវបានបិទ!!!').parent().addClass 'disabled'
      return
