TeamManagement.Common =
  init: ->
    @_initSelect2()
    @_initTableRowClick()

  _initSelect2: ->
    $('.select2').select2();

  _initTableRowClick: ->
    $('.table tbody tr.clickable-row').on 'click',(e) ->
      window.location = $(this).find('.action .action-url').data('path')
