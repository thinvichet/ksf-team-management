TeamManagement.ScanningsIndex =
  init: ->
    @_initDatePicker()
    @_hightlightInvalidRecord()

  _initDatePicker: ->
    $('.datepicker.created_at').datepicker(
      format: 'yyyy-mm-dd'
      todayHighlight: true
      startView: 'years'
      autoclose: true
      disableTouchKeyboard: true
    )

  _hightlightInvalidRecord: ->
    $('i.fa-times').parents('tr').css('background-color', '#f5b0b0')
