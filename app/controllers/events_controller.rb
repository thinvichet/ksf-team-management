class EventsController < ApplicationController
  before_action :set_game, only: [:index, :game_pdf]
  before_action :find_event, only: [:show, :join]
  def index
    game_sport_categories = GameSportCategory.where(sport_category_id: params[:sport_id], game: @game)

    @events = Event.joins(:game_sport_category_level).where(game_sport_category_levels: { game_sport_category_id: game_sport_categories.ids})
  end

  def show
    @team = Team.find_by(event: @event, user: current_user)
  end

  def game_pdf
    @game         = Game.current_active
    @sport        = SportCategory.find(params[:sport_id])
    @teams        = @game.teams.joins(:user).where( sport_category: @sport, users: {organization_id: current_user.organization.id})
    @non_athletes = Person.joins(:person_teams).non_athlete.where(person_teams: {team_id: @teams.ids} ).ordered_by_event_position

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Game_#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}",
          template: 'events/game_pdf',
          orientation: 'Landscape',
          encoding: 'utf8',
          layout: 'layouts/pdf_layout.haml',
          margin: { top: 5, left: 3, right: 3 }
        end
      end
  end

  def join
    @team = Team.find(params[:team_id])
    event_team = @event.event_teams.new(team: @team)
    if event_team.save
      redirect_to event_path(@event)
      flash[:notice] = I18n.t('notify.success_save')
    else
      redirect_to event_path(@event)
      flash[:alert] = I18n.t('notify.failed_save')
    end
  end

  protected

  def find_event
    @event = Event.find(params[:id])
  end

  def set_game
    @game = Game.find(params[:game_id])
  end
end
