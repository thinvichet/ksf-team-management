class PassportsController < ApplicationController
  before_action :find_person
  before_action :find_passport, only: [:edit, :update, :destroy]

  def new
    @passport = Passport.new
  end

  def create
    @passport = Passport.new(passport_params)
    if @passport.save
      redirect_to person_path(@person)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @passport.update_attributes(passport_params)
      redirect_to person_path(@person)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  private

  def find_person
    @person = Person.find(params[:person_id])
  end

  def find_passport
    @passport = Passport.find(params[:id])
  end

  def passport_params
    params.require(:passport).permit(:passport_picture, :passport_validity, :passport_number, :person_id)
  end
end
