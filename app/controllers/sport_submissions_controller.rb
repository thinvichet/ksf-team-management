class SportSubmissionsController < ApplicationController
  before_action :set_game, only: [:new, :create, :show, :game_submission]
  before_action :set_sport_submission, only: [:show, :game_submission]

  def new
    @sport_submission = @game.sport_submissions.new
    authorize @sport_submission
  end

  def create
    @sport_submission = @game.sport_submissions.new(sport_submission_params)
    authorize @sport_submission
    @sport_submission.organization = current_user.organization
    if @sport_submission.save
      redirect_to [@game, @sport_submission]
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
  end

  def game_submission
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Game_Submission_#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}",
          template: 'sport_submissions/game_submission',
          encoding: 'utf8',
          layout: 'layouts/pdf_layout.haml',
          margin: { top: 5, left: 3, right: 3 }
      end
    end
  end

  private

    def set_game
      @game = Game.find(params[:game_id])
    end

    def set_sport_submission
      @sport_submission = SportSubmission.find(params[:id])
    end

    def sport_submission_params
      params.require(:sport_submission).permit(:participant_amount, sport_category_ids: [])
    end
end
