class ScanningsController < ApplicationController
  def index
    if current_user.food_scanner? || current_user.accommodation_scanner?
      @scannings = ScanningGrid.new(params[:scanning_grid]) do |scope|
        scope.where(user_id: current_user.id)
      end
    else
      @scannings = ScanningGrid.new(params[:scanning_grid])
    end
    @scannings = @scannings.scope { |scope| scope.paginate(page: params[:page]) }
  end
end
