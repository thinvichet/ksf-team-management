class CardActivationsController < ApplicationController
  def index
    @card_activations = CardActivationGrid.new(params[:card_activation_grid]) do |scope|
      scope.paginate(page: params[:page])
    end
  end

  def activate
    @person = Person.find(params[:person_id])
    @activation = CardActivation.find_or_create_by(game: Game.current_active, person: @person, status: true) if @person.events.any?
  end
end
