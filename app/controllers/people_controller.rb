class PeopleController < ApplicationController
  before_action :find_person, only: [:show, :edit, :update, :destroy]
  before_action :authorize_user
  MISSIONARIES = ["ប្រធានប្រតិភូកីឡា(នាយបេសកកម្ម)", "អនុប្រធានប្រតិភូកីឡា(នាយបេសកកម្មរង)"]

  def index
    if current_user.admin? || current_user.readonly_admin?
      @people_grid = PersonGrid.new(params[:person_grid]) do |scope|
        scope.paginate(page: params[:page], per_page: 40)
      end
    else
      @people_grid = PersonGrid.new(params.fetch(:person_grid, {}).merge(current_user: current_user)) do |scope|
        scope.where('organization_id = (?) ', current_user.organization.id).paginate(page: params[:page], per_page: 40)
      end
    end

    respond_to do |format|
      format.html
      format.xlsx do
        send_data(@people_grid.to_spreadsheet, type: 'application/xlsx', filename: "តារាងកីឡាករ-#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}.xlsx")
      end
    end
  end

  def new
    @person = current_user.people.new
    @person.person_type = 'other' if current_user.sport_representer?
  end

  def create
    @person = current_user.people.new(person_params.merge(user_id: current_user.id))
    # add_card_type_to_person
    missionary = nil
    missionary = CardType.find(person_params[:card_type_id].to_i).name if person_params[:card_type_id].present?
    context = current_user.representer? ? :create_athlete : nil
    context = MISSIONARIES.include?(missionary) ? nil : context
    context = person_params[:from_organization].present? ? nil : context

    if @person.save(context: context)
      if params[:person][:avatar].present?
        render :crop
      else
        redirect_to person_path(@person)
        flash[:notice] = I18n.t('notify.success_save')
      end
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    @person.assign_attributes(person_params)
    # add_card_type_to_person
    missionary = nil

    missionary = CardType.find(person_params[:card_type_id].to_i).name if person_params[:card_type_id].present?
    context = current_user.representer? ? :create_athlete : nil
    context = MISSIONARIES.include?(missionary) ? nil : context
    context = person_params[:from_organization].present? ? nil : context

    if @person.save(context: context)
      if params[:person][:avatar].present?
        render :crop
      else
        redirect_to person_path(@person)
        flash[:notice] = I18n.t('notify.success_save')
      end
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
    respond_to do |format|
      format.html do
        @event_grid = EventGrid.new(){ |scope| scope = Event.where(id: @person.events.ids) } if @person.events.present?
      end
      format.pdf do
        render pdf: "Person_#{@person.id}_#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}",
                template: 'people/person_detail/person_detail.pdf.haml',
                encoding: 'utf8',
                layout:   'layouts/pdf_layout.haml',
                margin:   { top: 10, left: 3, right: 3 }
      end
    end
  end

  def destroy
    if @person.destroy
      redirect_to people_path()
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to person_path(@person)
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  def check_availibilities
    if current_user.activator? || current_user.admin?
      redirect_to activate_card_activations_path(person_id: params[:person_id])
    end

    @person = Person.find(params[:person_id])
    @is_valid = @person.has_valid_card?

    unless @is_valid
      scanning_type = current_user.scanning_type
      message       =
        if @person.events.any?
          if @person.events.where('started_at <= CURRENT_DATE').empty?
            'Event is not started'
          else
            'Card is not activated'
          end
        else
          'Not Compete'
        end

      last_scan     = Scanning.where(person: @person, game: Game.current_active).order(:id).last
      Scanning.create(user: current_user, person: @person, scanning_type: scanning_type, game: Game.current_active, succeed: @is_valid, message: message ) if last_scan.nil? || last_scan.created_at < 10.minutes.ago
    end
  end

  def confirm
    @is_valid       = true
    @person         = Person.find(params[:person_id])
    scanning_type   = current_user.scanning_type
    last_scan       = Scanning.find_by(person: @person, game: Game.current_active)
    Scanning.create(user: current_user, person: @person, scanning_type: scanning_type, game: Game.current_active, succeed: true ) if last_scan.nil? || last_scan.created_at < 10.minutes.ago

    flash[:notice]  = I18n.t('notify.success_save')
    render :check_availibilities
  end

  def team_member_card
    @game = Game.current_active
    @people = Person.where(id: params[:person_id]).order(updated_at: :desc)
    respond_to do |format|
      format.pdf do
        render pdf: 'card',
               template: 'people/card',
               page_size: 'A4',
               encoding: 'utf8',
               layout:   'layouts/pdf_layout.haml',
               margin:  { top:               6,
                          bottom:            6.5,
                          left:              5,
                          right:             5 }
      end
    end
  end

  private

  def authorize_user
    authorize controller_name.singularize.to_sym
  end

  def find_person
    if current_user.representer?
      @person = current_user.organization.people.find(params[:id])
    else
      @person = Person.find(params[:id])
    end
  end

  def person_params
    params.require(:person).permit(
      :crop_x,
      :crop_y,
      :crop_w,
      :crop_h,
      :sure_name_kh,
      :given_name_kh,
      :sure_name_en,
      :given_name_en,
      :identification_number,
      :citizenship,
      :nationality,
      :weight,
      :height,
      :blood_type,
      :address,
      :sex,
      :date_of_birth,
      :email,
      :phone,
      :avatar,
      :uniform_number,
      :person_type,
      :card_type_id,
      :sport_category_id,
      :organization_id,
      :user_id,
      :team_id,
      :from_organization
    )
  end

  def add_card_type_to_person
    @person.card_type == @person.athlete? ? 'F' : 'Fo' if current_user.representer?
  end
end
