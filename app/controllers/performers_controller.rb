class PerformersController < ApplicationController
  before_action :find_performer, only: [:show, :edit, :update, :destroy]
  before_action :authorize_user
  # skip_before_action :authenticate_user!, only: :check_availibilities

  def index
    if policy(:performer).index?
      @performers_grid = PerformerGrid.new(params[:performer_grid])
    else
      @performers_grid = PerformerGrid.new(params.fetch(:performer_grid, {}).merge(current_user: current_user)) do |scope|
         scope.where(organization: current_user.organization)
      end
    end
    # respond_to do |format|
    #   format.html
    #   format.xlsx {
    #     send_data(@people_grid.to_spreadsheet, type: 'application/xlsx', filename: "sockers-#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}.xlsx")
    #   }
    # end
  end

  def new
    @performer = Performer.new(user: current_user)
  end

  def create
    # @person = current_user.organization.people.new(person_params.merge(user_id: current_user.id))
    @performer = current_user.performers.new(performer_params)
    if @performer.save
      if params[:performer][:avatar].present?
        render :crop
      else
        redirect_to performers_url
        flash[:notice] = I18n.t('notify.success_save')
      end
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @performer.update_attributes(performer_params)
      if params[:performer][:avatar].present?
        render :crop
      else
        redirect_to performers_url
        flash[:notice] = I18n.t('notify.success_save')
      end
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
  #   respond_to do |format|
  #     format.html do
  #       @event_grid = EventGrid.new(){ |scope| scope = Event.where(id: @person.events.ids) } if @person.events.present?
  #     end
  #     format.pdf do
  #       render pdf: 'show',
  #               template: 'people/person_detail/person_detail.pdf.haml',
  #               encoding: 'utf8',
  #               layout:   'layouts/pdf_layout.haml',
  #               margin:   { top: 10, left: 3, right: 3 }
  #     end
  #   end
  end

  def destroy
    if @performer.destroy
      redirect_to performers_url
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to performer_url(@performer)
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  # def check_availibilities
  #   person = Person.find(params[:person_id])
  #   @is_valid = person.is_competed?
  # end

  def team_member_card
    @game = Game.current_active
    @performers = Performer.where(id: params[:performer_id])
    respond_to do |format|
      format.pdf do
        respond_to do |format|
          format.pdf do
            render pdf: 'card',
                   template: 'performers/card',
                   page_size: 'A4',
                   encoding: 'utf8',
                   layout:   'layouts/pdf_layout.haml',
                   margin:  { top:               6,
                              bottom:            6.5,
                              left:              5,
                              right:             5 }
          end
        end
      end
    end
  end


  private

  def authorize_user
    authorize controller_name.singularize.to_sym
  end

  def find_performer
    if current_user.representer?
      @performer = current_user.performers.find(params[:id])
    else
      @performer = Performer.find(params[:id])
    end
  end

  def performer_params
    params.require(:performer).permit(
      :sure_name_kh,
      :given_name_kh,
      :sure_name_en,
      :given_name_en,
      :identification_number,
      :citizenship,
      :nationality,
      :address,
      :sex,
      :date_of_birth,
      :email,
      :phone,
      :avatar,
      :user_id
    )
  end

  # def person_params
  #   params.require(:person).permit(
  #     :crop_x,
  #     :crop_y,
  #     :crop_w,
  #     :crop_h,
  #     :sure_name_kh,
  #     :given_name_kh,
  #     :sure_name_en,
  #     :given_name_en,
  #     :identification_number,
  #     :citizenship,
  #     :nationality,
  #     :weight,
  #     :height,
  #     :blood_type,
  #     :address,
  #     :sex,
  #     :date_of_birth,
  #     :email,
  #     :phone,
  #     :avatar,
  #     :uniform_number,
  #     :person_type,
  #     :sport_category_id,
  #     :user_id,
  #     :team_id
  #   )
  # end
end
