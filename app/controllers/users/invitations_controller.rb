class Users::InvitationsController < Devise::InvitationsController
  protected

  def invite_resource(&block)
    resource_class.invite!(invite_params, current_inviter, &block)
  end
end
