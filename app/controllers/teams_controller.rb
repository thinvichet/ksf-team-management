class TeamsController < ApplicationController
  before_action :find_event
  before_action :find_team, only: [:show, :edit, :update]

  def index
    if current_user.admin?
      @team_grid = TeamGrid.new()
    else
      @team_grid = TeamGrid.new() do |scope|
        scope.where(user_id: current_user.id)
      end
    end
  end

  def new
    @team = Team.new
    if @event.sport_category.uniform_restrict
       @event.sport_category.uniform_amount.times do
          @team.uniforms.build()
       end
    end
  end

  def create
    @team = Team.new(team_params.merge(user_id: current_user.id))
    if @team.save
      Notifier.alert_to(@team.sport_category.users.pluck(:phone), "[#{@team.sport_category.name}] - #{@team.name} registered to join #{@team.event.title}")
      redirect_to event_path(@event)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @team.update_attributes(team_params)
      redirect_to event_path(@event)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
    @people_grid = PersonGrid.new() do |scope|
      scope = Person.where(team: @team)
    end
  end

  private

  def find_event
    @event = Event.find(params[:event_id])
  end

  def find_team
    @team = Team.find(params[:id])
  end

  def team_params
    params.require(:team).permit(:name, :event_id, :sport_category_id, :event_id,
                                person_teams_attributes: [:id, :position, :measurement, :team_id, :person_id, :_destroy],
                                uniforms_attributes: [:id, :uniform_label, :value, :_destroy])
  end

end
