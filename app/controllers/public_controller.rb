class PublicController < ActionController::Base
  protect_from_forgery with: :exception
  layout "public"

  def check_availibilities
    # if current_user.activator? || current_user.admin?
    #   redirect_to activate_card_activations_path(person_id: params[:person_id])
    # end
    @person = Person.find(params[:person_id])
    # @is_valid = @person.has_valid_card?
    @is_valid = true
    #
    unless @is_valid
      scanning_type = current_user.scanning_type
      message       =
        if @person.events.any?
          if @person.events.where('started_at <= CURRENT_DATE').empty?
            'Event is not started'
          else
            'Card is not activated'
          end
        else
          'Not Compete'
        end

      last_scan     = Scanning.where(person: @person, game: Game.current_active).order(:id).last
      Scanning.create(user: current_user, person: @person, scanning_type: scanning_type, game: Game.current_active, succeed: @is_valid, message: message ) if last_scan.nil? || last_scan.created_at < 10.minutes.ago
    end
  end

  def team_member_card
    @game = Game.current_active
    @people = Person.where(id: params[:person_id]).only_allowed.order(updated_at: :desc)
    respond_to do |format|
      format.pdf do
        render pdf: 'card',
               template: 'public/card',
               page_size: 'A4',
               show_as_html: params.key?('debug'),
               encoding: 'utf8',
               layout:   'layouts/public.haml',
               margin:  { top:               6,
                          bottom:            6.5,
                          left:              5,
                          right:             5 }
      end
    end
  end
end
