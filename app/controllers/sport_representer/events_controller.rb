module SportRepresenter
  class EventsController < ApplicationController
    # before_action :authorize_user
    def index
      authorize :sport_representer_event
      @game = Game.current_active
      game_sport_categories = GameSportCategory.where(sport_category: current_user.sport_category, game: @game)

      @events = Event.joins(:game_sport_category_level).where(game_sport_category_levels: { game_sport_category: game_sport_categories})
    end

    def game_pdf
      @game = Game.current_active
      @sport = current_user.sport_category
      @teams = @game.teams.where( sport_category: @sport)
      @non_athletes = Person.joins(:person_teams).non_athlete.where(person_teams: {team_id: @teams.ids} ).ordered_by_event_position
      game_sport_categories = GameSportCategory.where(sport_category: current_user.sport_category, game: @game)

      respond_to do |format|
        format.html
        format.pdf do
          render pdf: "Event_Game_#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}",
            template: 'events/game_pdf',
            orientation: 'Landscape',
            encoding: 'utf8',
            layout: 'layouts/pdf_layout.haml',
            margin: { top: 5, left: 3, right: 3 }
          end
        end
    end

    def show
      authorize :sport_representer_event, :show?
      @event = Event.find(params[:id])
      @teams = Team.where(event: @event)
    end

    private

    def authorize_user
      authorize :sport_representer_event
    end
  end
end
