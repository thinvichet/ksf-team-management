module SportRepresenter
  class PeopleController < ApplicationController
    def index
      authorize :sport_representer_person
      @people_grid = SportRepresenterPersonGrid.new(params.fetch(:sport_representer_person_grid, {}).merge(current_user: current_user)) do
        |scope| scope.where("people.sport_category_id = ? OR people.user_id = ?", current_user.sport_category_id, current_user.id).references(:people)
      end
      respond_to do |format|
        format.html
        format.xlsx {
          send_data(@people_grid.to_spreadsheet, type: 'application/xlsx', filename: "people-#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}.xlsx")
        }
      end
    end
  end
end
