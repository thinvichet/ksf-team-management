module SportRepresenter
  class OrganizationsController < ApplicationController
    def index
      authorize :sport_representer_organization
      sport_ids = []
      sport_ids << current_user.sport_category.id
      sport_ids << SportCategory.only_children.where(parent: current_user.sport_category).ids
      sport_ids.flatten!
      @organizations_grid = SportRepresenterOrganizationGrid.new(params[:sport_representer_organizations_grid]) do |scope|
        scope.joins(sport_submissions: :submission_sport_lists).where(sport_submissions: { submission_sport_lists: { sport_category: sport_ids} }).page(params[:page])
      end

      respond_to do |format|
        format.html
        format.xlsx {
          send_data(@organizations_grid.to_spreadsheet(sport_ids), type: 'application/xlsx', filename: "របាយការណ៍ចុះឈ្មោះ-#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}.xlsx")
        }
      end
    end
  end
end
