class Admin::OrganizationsController < AdminBaseController
  before_action :set_organization, only: [:show, :edit, :update, :destroy]

  def index
    @organizations_grid = OrganizationsGrid.new(params[:organizations_grid]) do |scope|
      scope.page(params[:page])
    end
  end

  def new
    @organization = Organization.new
  end

  def edit
  end

  def create
    @organization = Organization.new(organization_params)

    respond_to do |format|
      if @organization.save
        format.html { redirect_to admin_organizations_url }
        flash[:notice] = I18n.t('notify.success_save')
      else
        format.html { render :new }
        flash[:notice] = I18n.t('notify.failed_save')
      end
    end
  end

  def update
    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to  admin_organizations_url}
        flash[:notice] = I18n.t('notify.success_save')
      else
        format.html { render :edit }
        flash[:notice] = I18n.t('notify.failed_save')
      end
    end
  end

  def destroy
    @organization.destroy
    respond_to do |format|
      format.html { redirect_to admin_organizations_url }
      flash[:notice] = I18n.t('notify.success_delete')
      format.json { head :no_content }
    end
  end

  private

  def set_organization
    @organization = Organization.find(params[:id])
  end

  def organization_params
    params.require(:organization).permit(:org_label, :name, :short_code, :logo)
  end
end
