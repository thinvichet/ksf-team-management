class Admin::GamesController < AdminBaseController
  before_action :set_game, only: %i[show edit update destroy]

  def index
    @game_grid = GameGrid.new(params[:game_grid])
  end

  def new
    @game = Game.new
  end

  def create
    @game = Game.new(game_params)
    if @game.save
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit; end

  def show
    @people_grid = PersonTeamGrid.new(params.fetch(:person_team_grid, {}).merge(current_user: current_user)) do |scope|
      scope.joins(:person_teams).distinct.where(person_teams: { id: @game.person_teams.ids }).where(id: @game.people).select('people.*, person_teams.position').page(params[:page])
    end
  end

  def update
    if @game.update_attributes(game_params)
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def destroy
    if @game.destroy
      redirect_to admin_games_path
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to admin_games_path
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def game_params
    params.require(:game).permit(:name, :year, :batch, :logo)
  end

  def set_game
    @game = Game.find(params[:id])
  end
end
