class Admin::NotificationsController < AdminBaseController
  before_action :set_sport, only: [:new, :create]

  def index
    @notifications = SportCategory.joins(:notifications).distinct
  end

  def new
    @notification = Notification.new
  end

  def create
    @notification = @sport.update(notification_param)
    if @notification.save
      redirect_to admin_notifications_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  private
    def notification_param
      params.require(:sport_category).permit()
    end

    def set_sport
      @sport = SportCategory.find(params[:sport_category_id])
    end

end
