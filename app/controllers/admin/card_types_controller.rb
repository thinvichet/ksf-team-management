class Admin::CardTypesController < AdminBaseController
  before_action :find_card_type, only: [:edit, :update, :show, :destroy]

  def index
    @card_type_grid = CardTypeGrid.new(params[:card_type_grid])
  end

  def new
    @card_type = CardType.new()
  end

  def create
    @card_type = CardType.new(card_type_params)
    if @card_type.save
      redirect_to admin_card_types_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @card_type.update_attributes(card_type_params)
      redirect_to admin_card_types_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def destroy
    if @card_type.destroy
      redirect_to admin_card_types_path
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to admin_card_types_path
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def find_card_type
    @card_type = CardType.find(params[:id])
  end

  def card_type_params
    params.require(:card_type).permit(:name, :letter, :allow_food)
  end
end
