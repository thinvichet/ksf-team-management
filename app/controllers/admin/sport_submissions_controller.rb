class Admin::SportSubmissionsController < AdminBaseController
  before_action :set_sport_submission
  before_action :set_organization
  def edit

  end

  def update
    if @sport_submission.update_attributes(sport_submission_params)
      redirect_to admin_organizations_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  private

  def sport_submission_params
    params.require(:sport_submission).permit(:participant_amount, sport_category_ids: [])
  end

  def set_organization
    @organization = Organization.find(params[:organization_id])
  end

  def set_sport_submission
    @sport_submission = SportSubmission.find(params[:id])
  end
end
