class Admin::SportCategoriesController < AdminBaseController
  before_action :find_sport_category, only: [:edit, :show, :update, :destroy]

  def index
    if params[:format] != 'xlsx'
      @sport_grid = SportGrid.new { |scope| scope = SportCategory.only_parent }
    else
      @sport_grid = SportGrid.new { |scope| scope }
    end

    respond_to do |format|
      format.html
      format.xlsx {
        send_data(@sport_grid.to_spreadsheet, type: 'application/xlsx', filename: "របាយការណ៍ចុះឈ្មោះ-#{Time.now.strftime("%V-%b-%y-%H-%M-%S")}.xlsx")
      }
    end
  end

  def edit

  end

  def update
    if @sport_category.update_attributes(sport_category_params)
      redirect_to admin_sport_categories_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def new
    @sport_category = SportCategory.new
  end

  def create
    @sport_category = SportCategory.new(sport_category_params)

    if @sport_category.save
      redirect_to admin_sport_categories_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
    # @events = Event.where(sport_category: @sport_category)
    @event_grid = EventGrid.new(){ |scope| scope = @sport_category.events }
  end

  def destroy
    if @sport_category.destroy
      redirect_to admin_sport_categories_path
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to admin_sport_categories_path
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def find_sport_category
    @sport_category = SportCategory.find(params[:id])
  end

  def sport_category_params
    params.require(:sport_category).permit(
      :name,
      :description,
      :logo,
      :uniform_restrict,
      :uniform_amount,
      :uniform_labels,
      :parent_id,
      :status,
      user_ids: [],
      levels_attributes: [:id, :level_number, :measurement_type, :_destroy])
  end
end
