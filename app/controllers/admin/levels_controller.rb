class Admin::LevelsController < AdminBaseController
  before_action :find_level, only: [:edit, :update, :destroy]
  before_action :find_sport

  def new
    @level = Level.new()
  end

  def create
    @level = Level.new(level_params)
    if @level.save
      redirect_to admin_sport_category_path(@sport)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @level.update_attributes(level_params)
      redirect_to admin_sport_category_path(@sport)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def destroy
    if @level.destroy
      redirect_to admin_sport_category_path(@sport)
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to admin_sport_category_path(@sport)
      # flash[alert] = "Failed to destroy level.\n #{@level.errors.messages.values.flatten.join(', ')}"
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def find_level
    @level = Level.find(params[:id])
  end

  def find_sport
    @sport = SportCategory.find(params[:sport_category_id])
  end

  def level_params
    params.require(:level).permit(:competition_name, :number_of_player, :sport_category_id)
  end
end
