class Admin::GameSportCategoriesController < AdminBaseController
  before_action :set_game
  before_action :set_game_sport_category, only: [:show, :edit, :update, :destroy]
  def new
    @game_sport_category = @game.game_sport_categories.new
  end

  def create
    @game_sport_category = @game.game_sport_categories.new(game_sport_category_params)
    if @game_sport_category.save
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit

  end

  def update
    if @game_sport_category.update_attributes(game_sport_category_params)
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def destroy
    if @game_sport_category.destroy
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def game_sport_category_params
    params.require(:game_sport_category).permit(:sport_category_id, level_ids:[])
  end

  def set_game_sport_category
    @game_sport_category = GameSportCategory.find(params[:id])
  end

  def set_game
    @game = Game.find(params[:game_id])
  end
end
