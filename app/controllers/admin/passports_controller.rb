class Admin::PassportsController < AdminBaseController

  before_action :find_passport, only: [:show, :verify, :invalid]

  def index
    @passport_grid = PassportGrid.new(params[:passport_grid])
  end

  def show

  end

  def verify
    if @passport.update_attributes(state: 'Verify')
      redirect_to admin_passports_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      redirect_to admin_passports_path
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def invalid
    if @passport.update_attributes(state: 'Invalid')
      redirect_to admin_passports_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      redirect_to admin_passports_path
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  private

  def find_passport
    @passport = Passport.find(params[:id])
  end
end
