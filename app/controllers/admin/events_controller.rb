class Admin::EventsController < AdminBaseController
  before_action :find_game
  before_action :find_event, only: [:edit, :update, :show, :destroy]
  def index

  end

  def new
    @event = Event.new()
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit
  end

  def update
    if @event.update_attributes(event_params)
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show
    @team_grid = TeamGrid.new(){ |scope| scope = Team.where(event: @event) }
  end

  def destroy
    if @event.destroy
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to admin_game_path(@game)
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def find_game
    @game = Game.find(params[:game_id])
  end

  def find_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:title, :age_from, :age_to, :gender_required, :weight_required, :started_at, :ended_at, :open_registered_at, :close_registered_at, :location, :description, :game_sport_category_level_id)
  end
end
