class Api::PeopleController < ApplicationController
  def find
    person = Person.find(params[:person_id])
    render json: person
  end
end
