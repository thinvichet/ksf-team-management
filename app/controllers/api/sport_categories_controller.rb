class Api::SportCategoriesController < ApplicationController
  def search
    sport = SportCategory.find(params[:sport_category_id])
    render json: sport
  end
end
