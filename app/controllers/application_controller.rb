class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception

  before_action :store_user_location!, if: :storable_location?
  before_action :set_locale
  before_action :authenticate_user!
  before_action :set_paper_trail_whodunnit
  after_action :prepare_unobtrusive_flash

  before_action :configure_permitted_parameters, if: :devise_controller?

  after_action :prepare_unobtrusive_flash

  layout :layout

  def default_url_options
    { locale: I18n.locale }
  end

  private

  def layout
    controller_name == 'sessions' ? 'devise_layout' : "application"
  end

  def set_locale
    if user_signed_in? && current_user.local.present?
      I18n.locale = current_user.local
    else
      I18n.locale = params[:locale] || I18n.default_locale
    end
  end

  protected

  rescue_from Pundit::NotAuthorizedError do |exception|
    redirect_to root_path, alert: 'could not access the page'
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(
      :invite, keys: [
        :skip_en_name_validation,
        :organization_id, :username,
        :sure_name_en, :given_name_en,
        :sure_name_kh, :given_name_kh,
        :role, :title
        ]
      )
    devise_parameter_sanitizer.permit(
      :accept_invitation, keys: [
        :skip_en_name_validation
      ]
    )
  end

  private

  def storable_location?
    request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
  end

  def store_user_location!
    store_location_for(:user, request.fullpath)
  end

end
