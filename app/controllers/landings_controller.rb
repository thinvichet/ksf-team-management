class LandingsController < ApplicationController
  def home
    if policy(:admin).index?
      redirect_to admin_games_path
    elsif current_user.sport_representer?
      redirect_to sport_representer_events_path
    elsif current_user.performer_manager?
      redirect_to performers_path
    elsif current_user.food_scanner? || current_user.accommodation_scanner?
      redirect_to scannings_path()
    elsif current_user.activator?
      redirect_to card_activations_path()
    else
      @game = Game.current_active
      @sport_submission = current_user.organization.sport_submissions.last
      if !Game.last.nil?
        if @sport_submission.try(:game) != @game
          redirect_to new_game_sport_submission_path(@game)
        else
          redirect_to game_sport_submission_path(@game, @sport_submission)
        end
      else
        redirect_to people_path()
      end
    end
  end
end
