class AdminBaseController < ApplicationController
  before_action :authorize_user

  private

  def authorize_user
    authorize :admin
  end
end
