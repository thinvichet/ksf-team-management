class CompetitionsController < ApplicationController
  before_action :find_competition, only: %i[edit update destroy]
  def index
    @competitions = Competition.all
  end

  def new
    @competition = Competition.new
  end

  def create
    @competition = Competition.new(competition_params)
    if @competition.save
      redirect_to competitions_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def edit; end

  def update
    if @competition.update_attributes(competition_params)
      redirect_to competitions_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:alert] = I18n.t('notify.failed_save')
    end
  end

  private

  def find_competition
    @competition = Competition.find(params[:id])
  end

  def competition_params
    params.require(:competition).permit(:name, :description, :sport_category_id)
  end
end
