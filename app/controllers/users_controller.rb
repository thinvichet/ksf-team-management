class UsersController < ApplicationController
  before_action :find_user, only: %i[disable edit show update destroy]

  def index
    @users_grid = UserGrid.new(params[:user_grid])
    authorize @users_grid.assets
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to users_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :new
      flash[:alert] = I18n.t('notify.failed_save')
    end
  end

  def edit; end

  def update
    if @user.update_attributes(user_params)
      redirect_to user_path(@user)
      flash[:notice] = I18n.t('notify.success_save')
    else
      render :edit
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def show; end

  def disable
    if @user.update(disabled: !@user.disabled)
      redirect_to users_path
      flash[:notice] = I18n.t('notify.success_save')
    else
      redirect_to users_path
      flash[:notice] = I18n.t('notify.failed_save')
    end
  end

  def destroy
    if @user.destroy
      redirect_to users_path
      flash[:notice] = I18n.t('notify.success_delete')
    else
      redirect_to users_path
      flash[:notice] = I18n.t('notify.failed_delete')
    end
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation,
      :username, :role, :phone, :organization_id, :sure_name_en, :given_name_en,
      :sure_name_kh, :given_name_kh, :title, :local, :sport_category_id)
  end
end
