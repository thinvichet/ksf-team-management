class Scanning < ApplicationRecord
  belongs_to :person
  belongs_to :user
  belongs_to :game

  delegate 	:name, 				to: :person, 	prefix: true, 	allow_nil: true
  delegate 	:fullname_kh, 
  					:fullname_en, to: :user, 		prefix: true, 	allow_nil: true
end
