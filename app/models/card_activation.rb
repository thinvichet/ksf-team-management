class CardActivation < ApplicationRecord
  belongs_to :person
  belongs_to :game

  def self.of_current_game
    find_by(game_id: Game.current_active)
  end

  def is_active?
    status
  end
end
