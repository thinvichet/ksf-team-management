class GameSportCategory < ApplicationRecord
  belongs_to :game
  belongs_to :sport_category

  has_many :game_sport_category_levels, dependent: :destroy
  has_many :levels, through: :game_sport_category_levels
  has_many :events, through: :game_sport_category_levels

  delegate :name, to: :sport_category, prefix: :sport
  
  validates :sport_category_id, uniqueness: { scope: :game_id }
end
