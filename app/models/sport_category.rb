class SportCategory < ApplicationRecord

  attr_accessor :order_num

  GENDER_STATUS = %w(Male Female).freeze

  has_one :user,      dependent: :nullify

  has_many :teams,    dependent: :nullify
  has_many :people,   dependent: :nullify
  has_many :person_sub_sport_categories, dependent: :nullify

  has_many :children, dependent: :destroy, class_name: 'SportCategory', foreign_key: :parent_id
  has_many :levels,   dependent: :destroy, inverse_of: :sport_category

  has_many :notifications, dependent: :destroy
  has_many :users,                              through: :notifications

  has_many :game_sport_categories, dependent: :destroy

  has_many :game,                         through: :game_sport_categories
  has_many :game_sport_category_levels,   through: :game_sport_categories
  has_many :events,                       through: :game_sport_category_levels

  has_many :submission_sport_lists
  has_many :sport_submissions, through: :submission_sport_lists
  has_many :organizations,     through: :sport_submissions

  belongs_to :parent, class_name: 'SportCategory', optional: true

  scope :only_parent,          -> { where(parent_id: nil) }
  scope :only_children,        -> { where.not(parent_id: nil) }
  scope :of_current_game,      -> { joins(:game_sport_categories).where(game_sport_categories: { game: Game.last }) }
  scope :joined_organizations, -> { of_current_game.joins(sport_submissions: :organization).includes(:organizations).merge(Organization.non_admin) }

  accepts_nested_attributes_for :users
  accepts_nested_attributes_for :children
  accepts_nested_attributes_for :levels, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true

  mount_uploader :logo, SportLogoUploader

  def self.filter(params)
    result = self

    result = result.where(id: params[:sport_category_id]) if params[:sport_category_id].present?

    result
  end

  def self.sport_submission_collection
    parent_ids = only_children.pluck(:parent_id).uniq
    select { |sport_category| parent_ids.exclude?(sport_category.id) }
  end

  def self.half_record_size
    ((all.size - 1) / 2) + 1
  end

  def self.first_half
    limit(half_record_size)
  end

  def self.second_half
    offset(half_record_size)
  end

  def decorated_uinform_labels
    uniform_labels.blank? ? [] : uniform_labels.split(',')
  end
end
