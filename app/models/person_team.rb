class PersonTeam < ApplicationRecord
  belongs_to :person
  belongs_to :team

  # NOT NEED RIGHT NOW
  scope :only_athlete, -> { where(position: 'Athletes')}
  scope :non_athlete, -> { where.not(position: 'Athletes')}
  validates :person_id, uniqueness: { scope: :team_id }
  validates :position, presence: true

  after_save :add_person_to_sub_sport

  SUB_SOCCER_MAN   = { sub_sport_id: 31, event_id: 85, main_sport_id: 3}
  SUB_SOCCER_WOMAN = { sub_sport_id: 32, event_id: 84, main_sport_id: 3}

  SUB_BASKETBALL_MAN   = { sub_sport_id: 33, event_id: 66, main_sport_id: 17}
  SUB_BASKETBALL_WOMAN = { sub_sport_id: 34, event_id: 67, main_sport_id: 17}

  SUB_VOLLEY_INSIDE_MAN   = { sub_sport_id: 35, event_id: 153, main_sport_id: 16}
  SUB_VOLLEY_INSIDE_WOMAN = { sub_sport_id: 36, event_id: 154, main_sport_id: 16}
  SUB_VOLLEY_OUTSIDE_MAN  = { sub_sport_id: 37, event_id: 155, main_sport_id: 16}

  POSITION = %w(Athletes Referees Coaches Medicals Delegates)
  POSITION_KH = %w(កីឡាករ អ្នកដឹកនាំ គ្រូបង្វឹក គ្រូបង្វឹកជំនាញ មន្ត្រីរដ្ឋាបាល)

  delegate :avatar, :name, :sex, :date_of_birth, :sport_category_id, :passport, to: :person, prefix: true, allow_nil: true

  delegate :level, to: :team

  def self.ordered_by_event_position
    query = "CASE "
    query << "WHEN position = 'Delegates' THEN 0 "
    query << "WHEN position = 'Referees' THEN 1 "
    query << "WHEN position = 'Coaches' THEN 2 "
    query << "WHEN position = 'Medicals' THEN 3 "
    query << "WHEN position = 'Athletes' THEN 4 "
    query << "END"
    order(query)
  end

  private

  def add_person_to_sub_sport

    pt = self

    return unless pt.team.sport_category_id.in?([3, 17, 16])
    if pt.team.event_id == SUB_SOCCER_MAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_SOCCER_MAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_SOCCER_WOMAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_SOCCER_WOMAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_BASKETBALL_MAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_BASKETBALL_MAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_BASKETBALL_WOMAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_BASKETBALL_WOMAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_VOLLEY_INSIDE_MAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_VOLLEY_INSIDE_MAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_VOLLEY_INSIDE_WOMAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_VOLLEY_INSIDE_WOMAN[:sub_sport_id])
    elsif pt.team.event_id == SUB_VOLLEY_OUTSIDE_MAN[:event_id]
      PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: SUB_VOLLEY_OUTSIDE_MAN[:sub_sport_id])
    end
  end

end
