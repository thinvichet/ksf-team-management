class SportSubmission < ApplicationRecord
  belongs_to :game
  belongs_to :organization

  has_many :submission_sport_lists, dependent: :destroy, inverse_of: :sport_submission

  has_many :sport_categories, through: :submission_sport_lists

  validates :submission_sport_lists, presence: true
  validates_associated :submission_sport_lists
  # validates :participant_amount, numericality: true, length: { minimum: 1 }

  def representer_sports
    parent_ids = sport_categories.pluck(:parent_id)
    category_ids = sport_categories.pluck(:id)
    SportCategory.where(id: category_ids + parent_ids).where(parent_id: nil)
  end
end
