class Event < ApplicationRecord
  belongs_to :game_sport_category_level

  has_many :teams, dependent: :nullify
  has_many :people, through: :teams

  STATE = %w(Open Close Start End)
  GENDER_REQUIRED = %w(both male female)

  validates :title, :open_registered_at, :close_registered_at, :started_at, :ended_at, :game_sport_category_level_id, presence: true
  # validates :game_sport_category_level, uniqueness: true
  validates :gender_required, inclusion: { in: GENDER_REQUIRED}

  delegate :level,                  to: :game_sport_category_level
  delegate :level_label,            to: :game_sport_category_level
  delegate :level_label_with_sport, to: :game_sport_category_level

  delegate :sport_name,     to: :game_sport_category_level, allow_nil: true
  delegate :sport_category, to: :game_sport_category_level
  delegate :game,           to: :game_sport_category_level

  delegate :number_of_player, to: :level
  delegate :competition_name, to: :level

  def state
    today = Date.today
    if open_registered_at < today
      if close_registered_at < today
        if started_at < today
          if ended_at < today
            return 'End'
          end
          return 'Start'
        end
        return 'Closed'
      end
      return 'Open'
    end
  end

  def self.current
    where('CURRENT_DATE BETWEEN started_at AND ended_at')
  end

  def self.started
    where('started_at <= CURRENT_DATE')
  end
end
