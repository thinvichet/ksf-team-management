class CardType < ApplicationRecord
  LETTER = %w(A B C D E F Fo G O O/C P V VIP VVIP S)

  has_many :people, dependent: :restrict_with_error
  has_many :performers, dependent: :restrict_with_error
end
