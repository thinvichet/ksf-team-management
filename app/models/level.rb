class Level < ApplicationRecord
  belongs_to :sport_category

  has_many :game_sport_category_levels, dependent: :restrict_with_error

  delegate :name, to: :sport_category, allow_nil: true, prefix: :sport

  scope :by_sport,     -> (sport_category_id) {where(sport_category_id: sport_category_id)}

  def level_label_with_sport
    "#{sport_name}: #{competition_name}"
  end

  def level_label
    "#{competition_name}"
  end
end
