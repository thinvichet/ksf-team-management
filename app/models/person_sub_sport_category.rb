class PersonSubSportCategory < ApplicationRecord
  belongs_to :person
  belongs_to :sport_category
end
