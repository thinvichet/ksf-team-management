class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :sport_category

  validates :user_id, uniqueness: { scope: :sport_category_id }
end
