class Passport < ApplicationRecord
  belongs_to :person

  mount_uploader :passport_picture, PassportPictureUploader

  STATE = %w(Pending Verify Invalid Expired)

  delegate :name, to: :person
end
