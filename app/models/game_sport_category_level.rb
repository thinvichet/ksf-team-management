class GameSportCategoryLevel < ApplicationRecord
  belongs_to :level
  belongs_to :game_sport_category

  has_one    :event

  delegate :event, to: :level

  delegate :level_label_with_sport, :sport_name, to: :level
  delegate :level_label, to: :level

  delegate :game,           to: :game_sport_category
  delegate :event,          to: :game_sport_category
  delegate :sport_category, to: :game_sport_category
end
