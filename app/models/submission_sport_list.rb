class SubmissionSportList < ApplicationRecord
  belongs_to :sport_submission
  belongs_to :sport_category
end
