class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_accessor :skip_password_validation, :skip_en_name_validation
  ROLES = %w(admin representer sport_representer readonly_admin performer_manager food_scanner accommodation_scanner activator)

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :invitable

  belongs_to :organization
  belongs_to :sport_category, optional: true

  has_many :teams
  has_many :notifications
  has_many :people,             dependent: :restrict_with_error
  has_many :performers,         dependent: :restrict_with_error
  has_many :events,             through: :teams
  has_many :sport_categories,   through: :notifications

  validates :role, inclusion: ROLES
  validates :role, :organization_id, :sure_name_kh, :given_name_kh, :title, presence: true
  validates :sure_name_en, :given_name_en, presence: true, if: :skip_en_name_validation?
  validates :sport_category_id, presence: true, if: :sport_representer?

  delegate :name,       to: :organization, allow_nil: true, prefix: :org
  delegate :org_label,  to: :organization, allow_nil: true
  delegate :short_code, to: :organization, allow_nil: true, prefix: :org

  delegate :name,       to: :sport_category, allow_nil: true, prefix: true


  def fullname_kh
    "#{sure_name_kh} #{given_name_kh}"
  end

  def fullname_en
    "#{sure_name_en} #{given_name_en}"
  end

  def readonly_admin?
    role == 'readonly_admin'
  end

  def sport_representer?
    role == 'sport_representer'
  end

  def admin?
    role == 'admin'
  end

  def representer?
    role == 'representer'
  end

  def performer_manager?
    role == 'performer_manager'
  end

  def food_scanner?
    role == 'food_scanner'
  end

  def accommodation_scanner?
    role == 'accommodation_scanner'
  end

  def activator?
    role == 'activator'
  end

  def scanning_type
    if food_scanner?
      'Food'
    elsif accommodation_scanner?
      'Accommodation'
    else
      nil
    end
  end

  def active_for_authentication?
   super and !self.disabled?
  end

  protected

  def skip_en_name_validation?
    return false if skip_en_name_validation
    true
  end

  def password_required?
    return false if skip_password_validation
    super
  end
end
