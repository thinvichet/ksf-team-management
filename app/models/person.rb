class Person < ApplicationRecord

  mount_uploader :avatar, AvatarUploader

  PERSON_TYPE = ['athlete', 'referee', 'nf delegate', 'if delegate', 'other']

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h

  belongs_to :user
  belongs_to :sport_category, optional: true
  belongs_to :organization
  belongs_to :card_type

  has_one :passport,      dependent: :destroy

  has_many :events,       through: :teams
  has_many :teams,        through: :person_teams
  has_many :person_teams, inverse_of: :person,  dependent: :restrict_with_error

  has_many :card_activations,       dependent: :destroy
  has_many :person_sub_sport_categories, dependent: :destroy

  delegate :name,                   to: :sport_category,  prefix: true, allow_nil: true
  delegate :name,                   to: :organization,    prefix: true, allow_nil: true
  delegate :passport_status,        to: :passport,        prefix: true, allow_nil: true
  delegate :passport_validity,      to: :passport,                      allow_nil: true
  delegate :username,               to: :user,            prefix: true

  scope :without_team,         ->        { where(team_id: nil)               }
  scope :by_sport,             ->(value) { where(sport_category_id: value)   }
  scope :by_event,             ->(value) { where(event_id: value)            }
  scope :only_athlete,         ->        { where(person_type: 'athlete')     }
  scope :only_referee,         ->        { where(person_type: 'referee')     }
  scope :only_other,           ->        { where(person_type: 'other')       }
  scope :non_athlete,          ->        { where.not(person_type: 'athlete') }

  validates :sex,                   inclusion: %w(Male Female)
  validates :person_type,           inclusion: PERSON_TYPE
  validates :organization_id,       presence: true
  validates :sex,                   presence: true
  validates :phone,                 presence: true
  validates :person_type,           presence: true
  validates :card_type_id,             presence: true
  validates :sure_name_en,          presence: true
  validates :given_name_en,         presence: true
  validates :date_of_birth,         presence: true
  validates :sport_category_id,     presence: true, on: :create_athlete
  validates :height,                presence: true
  validates :weight,                presence: true
  validates :citizenship,           presence: true
  validates_length_of :identification_number, minimum: 9, maximum: 11, allow_blank: true
  validates :sure_name_kh, uniqueness: { scope: [:given_name_kh, :date_of_birth, :phone] }

  after_update :crop_avatar

  def self.ordered_by_event_position
    query = "CASE "
    query << "WHEN person_teams.position = 'Delegates' THEN 0 "
    query << "WHEN person_teams.position = 'Referees' THEN 1 "
    query << "WHEN person_teams.position = 'Coaches' THEN 2 "
    query << "WHEN person_teams.position = 'Medicals' THEN 3 "
    query << "WHEN person_teams.position = 'Athletes' THEN 4 "
    query << "END"
    order(query)
  end

  def self.by_gender(gender)
    if gender == 'Male'
      where(sex: 'Male')
    elsif gender == 'Female'
      where(sex: 'Female')
    else
      where(sex: ['Male', 'Female'])
    end
  end

  def self.by_weight(value)
    return self if value.nil? || value.zero?
    where(weight: value)
  end

  def self.athlete_by_rule(event)
    athlete_ids = Person.by_gender(event.gender_required.titleize)
      .by_weight(event.weight_required)
      .select{|person| person.age >= event.age_from && person.age <= event.age_to }
      .map(&:id)
    only_athlete.where(id: athlete_ids)
  end

  def crop_avatar
    avatar.recreate_versions! if crop_x.present?
  end

  def athlete?
    person_type == 'athlete'
  end

  def referee?
    person_type == 'referee'
  end

  def non_athlete?
    person_type != 'athlete'
  end

  def nf_delegate?
    person_type == 'nf delegate'
  end

  def if_delegate?
    person_type == 'if delegate'
  end

  def age
    (Time.now.to_s(:number).to_i - date_of_birth.to_time.to_s(:number).to_i)/10e9.to_i
  end

  def name
    "#{sure_name_kh} #{given_name_kh}"
  end

  def name_en
    "#{given_name_en} #{sure_name_en}"
  end

  def position_in(team)
    person_teams.where(team: team).pluck(:position)
  end

  def profile_pic_expire?
    return true if avatar.blank?
    uploaded_at = DateTime.strptime(avatar.timestamp.to_s,'%s')
    expired_at = uploaded_at + 2.years
    expired_at <= DateTime.now
  end

  def has_valid_card?
    if card_type.letter.in?(['A', 'B']) && !user.sport_representer?
      true
    elsif organization_name.in?(['សហព័ន្ធកីឡាទ្រីយ៉ាត្លុងកម្ពុជា', 'សហព័ន្ធខ្មែរកីទ្បាទោចក្រយាន', 'កីឡាវាយកូនបាល់កម្ពុជា'])
      true
    else
      events.any? && card_activations.of_current_game.try(:is_active?)
    end
  end
end
