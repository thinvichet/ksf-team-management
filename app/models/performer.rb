class Performer < ApplicationRecord
  belongs_to :user
  belongs_to :card_type

  mount_uploader :avatar, AvatarUploader

  before_validation :assign_performer_card_type

  validates :sex,                   inclusion: %w(Male Female)
  validates :sex,                   presence: true
  validates :phone,                 presence: true
  validates :sure_name_kh,          presence: true
  validates :given_name_kh,         presence: true
  validates :date_of_birth,         presence: true
  validates :citizenship,           presence: true
  validates_length_of :identification_number, minimum: 9, maximum: 10, allow_blank: true
  validates :sure_name_kh, uniqueness: { scope: [:given_name_kh, :date_of_birth, :phone] }

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h

  after_update :crop_avatar

  def name
    "#{sure_name_kh} #{given_name_kh}"
  end

  def crop_avatar
    avatar.recreate_versions! if crop_x.present?
  end

  def age
    (Time.now.to_s(:number).to_i - date_of_birth.to_time.to_s(:number).to_i)/10e9.to_i
  end

  private

  def assign_performer_card_type
    self.card_type = CardType.find_by(letter: 'O/C')
  end
end
