class Game < ApplicationRecord
  mount_uploader :logo, LogoUploader

  has_many :game_sport_categories, inverse_of: :game, dependent: :restrict_with_error
  has_many :sport_submissions,                        dependent: :restrict_with_error
  has_many :card_activations,                         dependent: :destroy

  has_many :levels,                     through: :game_sport_categories
  has_many :sport_categories,           through: :game_sport_categories
  has_many :game_sport_category_levels, through: :game_sport_categories
  has_many :events,                     through: :game_sport_category_levels
  has_many :teams,                      through: :events
  has_many :person_teams,               through: :teams
  has_many :people,                     through: :person_teams

  accepts_nested_attributes_for :sport_submissions, reject_if: :all_blank, allow_destroy: true

  validates :name, :year, :batch, presence: true

  # accepts_nested_attributes_for :game_sport_categories, reject_if: :all_blank, allow_destroy: true
  # accepts_nested_attributes_for :game_sport_category_levels

  def self.current_active
    last
  end
end
