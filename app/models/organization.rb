class Organization < ApplicationRecord
  mount_uploader :logo, LogoUploader

  has_many :users
  has_many :sport_submissions
  has_many :people

  scope :non_admin, -> { where.not(name: 'Admin Team') }

  validates :org_label, :name, :short_code, presence: true

  scope :only_organization_reprenter, -> { joins(:users).where('users.role = ?', 'representer').uniq }

  def get_sport_submission
    sport_submissions.last
  end
end
