class Team < ApplicationRecord
  attr_accessor :gender

  belongs_to :sport_category
  belongs_to :user
  belongs_to :event

  has_many   :person_teams, inverse_of: :team, dependent: :destroy
  has_many   :people, through: :person_teams

  has_many :uniforms, inverse_of: :team

  accepts_nested_attributes_for :person_teams, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :uniforms, reject_if: :all_blank, allow_destroy: true

  validates :name,              presence: true
  validates :sport_category_id, presence: true

  validate  :exact_member_amount
  validate  :athlete_gender
  validate  :duplicate_person

  delegate :id,   to: :sport_category, prefix: true, allow_nil: true
  delegate :name, to: :sport_category, prefix: true, allow_nil: true

  delegate :level,            to: :event
  delegate :number_of_player, to: :event, prefix: :level

  private

  def exact_member_amount
    return if level_number_of_player >= person_teams.reject(&:marked_for_destruction?).select{|person_team| person_team.position == 'Athletes'}.count
    errors.add(:name, I18n.t('.errors.messages.team.member_amount', count: level_number_of_player))
  end

  def athlete_gender
    return if event.gender_required == 'both'
    person_teams.reject(&:marked_for_destruction?).each do |person_team|
      return if person_team.position != 'Athletes'
      if event.gender_required != person_team.person.sex.downcase
        errors.add(:name, 'the athlete gender must be match')
        person_team.errors.add(:gender, 'gender unmatch')
      end
    end
  end

  def duplicate_person
    grouped_person = person_teams.reject(&:marked_for_destruction?).group_by{ |person_team| [person_team.person_id]}
    duplicated_person = grouped_person.values.select { |person| person.size > 1 }.flatten

    return unless duplicated_person.any?
    errors.add(:name, I18n.t('.errors.messages.team.name'))

    duplicated_person.each do |person_team|
      person_team.errors.add(:person_id, "#{I18n.t('.errors.messages.team.person_id')} #{person_team.position}")
      person_team.errors.add(:position, I18n.t('.errors.messages.team.position'))
    end
  end
end
