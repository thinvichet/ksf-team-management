Rails.application.routes.draw do
  scope '(:locale)', locale: /en|kh/ do
    get 'sports/index', as: 'sports'
    resources :sports, only: :show do
      resources :events
    end

    resources :games do
      resources :sport_submissions do
        get :game_submission, on: :member
      end
      resources :events do
        get :game_pdf, on: :collection
      end
    end

    root 'landings#home'
    devise_for :users, session: 'users/sessions'

    namespace :admin do
      resources :organizations, except: :show do
        resources :sport_submissions, except: [:new, :create, :destroy]
      end
      resources :games do
        resources :events
        resources :game_sport_categories, except: :show
      end
      resources :notifications, only: :index
      resources :people
      resources :passports do
        member do
          get :verify
          get :invalid
        end
      end
      resources :sport_categories do
        resources :levels
        resources :notifications, only: %i[new create]
      end

      resources :card_types
    end

    resources :competitions
    resources :events, only: %i[index show] do
      resources :teams
      put :join, on: :member
    end
    resources :users do
      get :disable, on: :member
    end
    resources :performers do
      get :team_member_card,      on: :collection
    end

    get '/people/:person_id/check_qr', to: 'public#check_availibilities', as: 'person_check_availibilities'
    resources :people do
      resources :passports
      # get :check_availibilities
      post :confirm
      get :team_member_card,      on: :collection
    end
    namespace :sport_representer do
      resources :events, except: %i[new create edit update destroy] do
        get :game_pdf, on: :collection
      end
      resources :people, only: [:index, :show]
      resources :organizations, only: :index
    end

    resources :scannings, only: :index
    resources :card_activations, only: :index do
      get :activate, on: :collection
    end
  end
  namespace :api do
    resources :sport_categories, only: [:search] do
      get :search, on: :collection
    end
    resources :people, only: %i[show find] do
      collection do
        get :find
      end
    end
  end
end
