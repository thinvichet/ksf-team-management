namespace :database_dump do
  desc "Update all users password to '123456789'"
  task user_password: :environment do
    puts "update password of all user into 123456789"
    User.all.update(password: '123456789')
  end

  desc "Clear Sport logo and Avatar picture"
  task clear_images: :environment do
    Person.all.each do |person|
      puts "clear person avatar of #{person.name}"
      person.update_columns(avatar: '')
    end
    SportCategory.all.each do |sport|
      puts "clear sport logo of #{sport.name}"
      sport.update!(logo: '')
    end
  end

end
