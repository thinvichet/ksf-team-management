# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190619143300) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "athletes", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "nationality"
    t.string   "sex"
    t.text     "address"
    t.date     "date_of_birth"
    t.integer  "team_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "avatar"
    t.text     "documents"
    t.index ["team_id"], name: "index_athletes_on_team_id", using: :btree
  end

  create_table "card_activations", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "game_id"
    t.boolean  "status",     default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["game_id"], name: "index_card_activations_on_game_id", using: :btree
    t.index ["person_id"], name: "index_card_activations_on_person_id", using: :btree
  end

  create_table "card_types", force: :cascade do |t|
    t.string   "name"
    t.string   "letter"
    t.boolean  "allow_food", default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "competition_teams", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "competition_id"
    t.integer  "team_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["competition_id"], name: "index_competition_teams_on_competition_id", using: :btree
    t.index ["event_id"], name: "index_competition_teams_on_event_id", using: :btree
    t.index ["team_id"], name: "index_competition_teams_on_team_id", using: :btree
  end

  create_table "competitions", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sport_category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["sport_category_id"], name: "index_competitions_on_sport_category_id", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.text     "location"
    t.text     "description"
    t.date     "started_at"
    t.date     "ended_at"
    t.integer  "sport_category_id"
    t.integer  "competition_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.date     "open_registered_at"
    t.date     "close_registered_at"
    t.integer  "game_sport_category_level_id"
    t.string   "gender_required",              default: "both"
    t.integer  "age_from",                     default: 0
    t.integer  "age_to",                       default: 100
    t.float    "weight_required",              default: 0.0
    t.index ["competition_id"], name: "index_events_on_competition_id", using: :btree
    t.index ["game_sport_category_level_id"], name: "index_events_on_game_sport_category_level_id", using: :btree
    t.index ["sport_category_id"], name: "index_events_on_sport_category_id", using: :btree
  end

  create_table "game_sport_categories", force: :cascade do |t|
    t.integer  "game_id"
    t.integer  "sport_category_id"
    t.date     "started_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["game_id"], name: "index_game_sport_categories_on_game_id", using: :btree
    t.index ["sport_category_id"], name: "index_game_sport_categories_on_sport_category_id", using: :btree
  end

  create_table "game_sport_category_levels", force: :cascade do |t|
    t.integer  "game_sport_category_id"
    t.integer  "level_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["game_sport_category_id"], name: "index_game_sport_category_levels_on_game_sport_category_id", using: :btree
    t.index ["level_id"], name: "index_game_sport_category_levels_on_level_id", using: :btree
  end

  create_table "games", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "logo"
    t.integer  "year"
    t.integer  "batch"
  end

  create_table "levels", force: :cascade do |t|
    t.float    "level_number",      default: 0.0
    t.string   "measurement_type",  default: ""
    t.integer  "sport_category_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.float    "level_from",        default: 0.0
    t.float    "level_to",          default: 0.0
    t.string   "competition_name"
    t.integer  "number_of_player",  default: 1
    t.index ["sport_category_id"], name: "index_levels_on_sport_category_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "sport_category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["sport_category_id"], name: "index_notifications_on_sport_category_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string   "org_label"
    t.string   "name"
    t.string   "short_code"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "logo",       default: ""
  end

  create_table "passports", force: :cascade do |t|
    t.string   "passport_picture",  default: ""
    t.string   "passport_number",   default: ""
    t.date     "passport_validity"
    t.string   "nationality",       default: ""
    t.string   "state",             default: "Pending"
    t.integer  "person_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["person_id"], name: "index_passports_on_person_id", using: :btree
  end

  create_table "people", force: :cascade do |t|
    t.string   "sure_name_kh"
    t.string   "given_name_kh"
    t.string   "sex"
    t.string   "email"
    t.string   "phone"
    t.string   "person_type"
    t.string   "avatar"
    t.integer  "user_id"
    t.integer  "sport_category_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.date     "date_of_birth"
    t.string   "sure_name_en"
    t.string   "given_name_en"
    t.float    "height"
    t.float    "weight"
    t.string   "blood_type"
    t.string   "nationality"
    t.string   "citizenship"
    t.string   "address"
    t.string   "identification_number", default: ""
    t.integer  "organization_id"
    t.string   "uniform_number",        default: ""
    t.integer  "card_type_id"
    t.string   "from_organization",     default: ""
    t.index ["card_type_id"], name: "index_people_on_card_type_id", using: :btree
    t.index ["organization_id"], name: "index_people_on_organization_id", using: :btree
    t.index ["sport_category_id"], name: "index_people_on_sport_category_id", using: :btree
    t.index ["sure_name_kh", "given_name_kh", "date_of_birth", "phone", "identification_number"], name: "index_people_on_name_kh_dob_phone_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_people_on_user_id", using: :btree
  end

  create_table "performers", force: :cascade do |t|
    t.string   "sure_name_kh"
    t.string   "given_name_kh"
    t.string   "sex"
    t.string   "email"
    t.string   "phone"
    t.string   "person_type"
    t.string   "avatar"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.date     "date_of_birth"
    t.string   "sure_name_en"
    t.string   "given_name_en"
    t.string   "nationality"
    t.string   "citizenship"
    t.string   "address"
    t.string   "identification_number", default: ""
    t.integer  "user_id"
    t.integer  "card_type_id"
    t.index ["card_type_id"], name: "index_performers_on_card_type_id", using: :btree
    t.index ["user_id"], name: "index_performers_on_user_id", using: :btree
  end

  create_table "person_sub_sport_categories", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "sport_category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["person_id"], name: "index_person_sub_sport_categories_on_person_id", using: :btree
    t.index ["sport_category_id"], name: "index_person_sub_sport_categories_on_sport_category_id", using: :btree
  end

  create_table "person_teams", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "team_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "position",    default: ""
    t.float    "measurement", default: 0.0
    t.index ["person_id"], name: "index_person_teams_on_person_id", using: :btree
    t.index ["team_id"], name: "index_person_teams_on_team_id", using: :btree
  end

  create_table "scannings", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "user_id"
    t.string   "scanning_type"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "game_id"
    t.boolean  "succeed",       default: false
    t.string   "message",       default: ""
    t.index ["game_id"], name: "index_scannings_on_game_id", using: :btree
    t.index ["person_id"], name: "index_scannings_on_person_id", using: :btree
    t.index ["user_id"], name: "index_scannings_on_user_id", using: :btree
  end

  create_table "sport_categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "logo"
    t.boolean  "uniform_restrict", default: false
    t.string   "uniform_labels"
    t.integer  "uniform_amount",   default: 2
    t.integer  "parent_id"
    t.string   "status",           default: ""
  end

  create_table "sport_submissions", force: :cascade do |t|
    t.integer  "participant_amount"
    t.integer  "game_id"
    t.integer  "organization_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["game_id"], name: "index_sport_submissions_on_game_id", using: :btree
    t.index ["organization_id"], name: "index_sport_submissions_on_organization_id", using: :btree
  end

  create_table "submission_sport_lists", force: :cascade do |t|
    t.integer  "sport_submission_id"
    t.integer  "sport_category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["sport_category_id"], name: "index_submission_sport_lists_on_sport_category_id", using: :btree
    t.index ["sport_submission_id"], name: "index_submission_sport_lists_on_sport_submission_id", using: :btree
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.integer  "sport_category_id"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "event_id"
    t.index ["event_id"], name: "index_teams_on_event_id", using: :btree
    t.index ["sport_category_id"], name: "index_teams_on_sport_category_id", using: :btree
    t.index ["user_id"], name: "index_teams_on_user_id", using: :btree
  end

  create_table "uniforms", force: :cascade do |t|
    t.string   "uniform_label"
    t.string   "value"
    t.integer  "team_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["team_id"], name: "index_uniforms_on_team_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "username"
    t.string   "role"
    t.boolean  "disabled",               default: false
    t.string   "phone",                  default: ""
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "organization_id"
    t.string   "sure_name_en"
    t.string   "given_name_en"
    t.string   "sure_name_kh"
    t.string   "given_name_kh"
    t.string   "title"
    t.string   "local"
    t.integer  "sport_category_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
    t.index ["organization_id"], name: "index_users_on_organization_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["sport_category_id"], name: "index_users_on_sport_category_id", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

  add_foreign_key "athletes", "teams"
  add_foreign_key "card_activations", "games"
  add_foreign_key "card_activations", "people"
  add_foreign_key "competition_teams", "competitions"
  add_foreign_key "competition_teams", "events"
  add_foreign_key "competition_teams", "teams"
  add_foreign_key "competitions", "sport_categories"
  add_foreign_key "events", "competitions"
  add_foreign_key "events", "game_sport_category_levels"
  add_foreign_key "events", "sport_categories"
  add_foreign_key "game_sport_categories", "games"
  add_foreign_key "game_sport_categories", "sport_categories"
  add_foreign_key "game_sport_category_levels", "game_sport_categories"
  add_foreign_key "game_sport_category_levels", "levels"
  add_foreign_key "levels", "sport_categories"
  add_foreign_key "notifications", "sport_categories"
  add_foreign_key "notifications", "users"
  add_foreign_key "passports", "people"
  add_foreign_key "people", "card_types"
  add_foreign_key "people", "organizations"
  add_foreign_key "people", "sport_categories"
  add_foreign_key "people", "users"
  add_foreign_key "performers", "card_types"
  add_foreign_key "person_sub_sport_categories", "people"
  add_foreign_key "person_sub_sport_categories", "sport_categories"
  add_foreign_key "person_teams", "people"
  add_foreign_key "person_teams", "teams"
  add_foreign_key "scannings", "games"
  add_foreign_key "scannings", "people"
  add_foreign_key "scannings", "users"
  add_foreign_key "sport_submissions", "games"
  add_foreign_key "sport_submissions", "organizations"
  add_foreign_key "submission_sport_lists", "sport_categories"
  add_foreign_key "submission_sport_lists", "sport_submissions"
  add_foreign_key "teams", "events"
  add_foreign_key "teams", "sport_categories"
  add_foreign_key "teams", "users"
  add_foreign_key "uniforms", "teams"
  add_foreign_key "users", "organizations"
  add_foreign_key "users", "sport_categories"
end
