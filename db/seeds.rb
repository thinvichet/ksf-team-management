# organization = Organization.find_or_create_by(org_label: 'Admin', name: 'Admin Team', short_code: 'ADMIN')

# User.create(email: 'admin@dgos.com', username: 'admin', role: 'admin',
#             phone: '+855963422224', sure_name_en: 'Thin', given_name_en: 'Vichet', sure_name_kh: 'ធិន',
#             given_name_kh: 'វិចិត្រ', title: 'System Admin', local: 'kh', organization: organization, password: '1234567890')

# user = User.create(email: 'user@email.com', username: 'User', role: 'representer',
#             phone: '+855963422224', sure_name_en: 'User', given_name_en: 'User', sure_name_kh: 'ធិន',
#             given_name_kh: 'វិចិត្រ', title: 'Representer', local: 'kh', organization: organization, password: '1234567890')

# # user = User.where(role: 'representer').first;
SportCategory.find_or_create_by(name: 'ហែលទឹក');
sport_category_ids = SportCategory.all.pluck(:id);
# team_names = [
#               ['Sok kakada', 'សុខ កក្កដា'],
#               ['Rours Vit', 'រស់ វិត'],
#               ['Chek Chouch', 'ចេក ចូច'],
#               ['Churb pdeng', 'ជួប ប្តឹង'],
#               ['Chan Samai', 'ចាន់ សម័យ'],
#               ['Meas Saly', 'មាស សាលី'],
#               ['Yours Rachana', 'យួស រាចនា'],
#               ['Hem Hoeun', 'ហែម ហឿន'],
#               ['Long Rarath', 'ឡុង សារដ្ឍ'],
#               ['Chit Peang', 'ជិត ពាង'],
#               ['Lurch Merl', 'លួច មើល'],
#               ['Sam Pisak', 'សម ពិសាក់'],
#             ]

athlete   = CardType.find_or_create_by(name: 'Athlete', letter: 'F')
referee   = CardType.find_or_create_by(name: 'មន្រ្តីអាជ្ញាកណ្តាល', letter: 'Fo')
coach     = CardType.find_or_create_by(name: 'គ្រូបង្វឹក', letter: 'Fo')
medical   = CardType.find_or_create_by(name: 'ពេទ្យ', letter: 'Fo')
delegate  = CardType.find_or_create_by(name: 'មន្រ្តីរដ្ឋបាល', letter: 'Fo')
team_names = (1..50).map do |_|
  [FFaker::Name.first_name, FFaker::Name.last_name]
end
team_names.each do |first_name, last_name|
  Person.create!(
    # sure_name_kh: kh_spit_name.first,
    # given_name_kh: kh_spit_name.last,
    sure_name_en: last_name,
    given_name_en: first_name,
    sex: ['Male', 'Female'].sample,
    email: "#{first_name.downcase}@example.com",
    phone: "#{['012', '010', '070', '099'].sample} #{rand.to_s[2..7]}",
    date_of_birth: Time.at(15 + rand * (Time.now.to_f - 18.to_f)),
    user_id: User.last.id,
    person_type: "athlete",
    card_type: athlete,
    sport_category_id: sport_category_ids.first,
    organization_id: Organization.last.id,
    identification_number: rand.to_s[0..9],
    height: rand(60.0...80.9),
    weight: rand(50.2...76.9),
    nationality: 'Khmer',
    citizenship: 'Cambodian',
    address: 'PP',
    blood_type: "#{['O', 'AB', 'A', 'B'].sample}"
  )
end

User.last.people.each do |person|
  card_type =
    if person.athlete?
      athlete
    elsif person.referee?
      referee
    else
      role_in_event = person.position_in(Game.current_active.teams).first
      return nil if role_in_event.nil?
      if role_in_event == 'Coaches'
        coach
      elsif role_in_event == 'Medicals'
        medical
      elsif role_in_event == 'Delegates'
        delegate
      end
    end

  person.update(card_type: card_type)
end
