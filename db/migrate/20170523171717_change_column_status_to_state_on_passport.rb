class ChangeColumnStatusToStateOnPassport < ActiveRecord::Migration[5.0]
  def change
      change_column :passports, :status, :string, default: 'Pending'
      rename_column :passports, :status, :state
  end
end
