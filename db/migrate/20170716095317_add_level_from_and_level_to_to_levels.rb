class AddLevelFromAndLevelToToLevels < ActiveRecord::Migration[5.0]
  def change
    add_column :levels, :level_from, :float, default: 0.0
    add_column :levels, :level_to, :float, default: 0.0
  end
end
