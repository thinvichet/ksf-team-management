class AddPersonToSubSportCategoryByEvent < ActiveRecord::Migration[5.0]
  def change
    sub_soccer_man = { sub_sport_id: 31, event_id: 85, main_sport_id: 3}
    sub_soccer_woman  = { sub_sport_id: 32, event_id: 84, main_sport_id: 3}

    sub_basketball_man = { sub_sport_id: 33, event_id: 66, main_sport_id: 17}
    sub_basketball_woman = { sub_sport_id: 34, event_id: 67, main_sport_id: 17}

    sub_volley_inside_man = { sub_sport_id: 35, event_id: 153, main_sport_id: 16}
    sub_volley_inside_woman = { sub_sport_id: 36, event_id: 154, main_sport_id: 16}
    sub_volley_outside_man = { sub_sport_id: 37, event_id: 155, main_sport_id: 16}

    PersonTeam.joins(:team).where(teams: {sport_category_id:  [3, 17, 16]}).each do |pt|
      if pt.team.event_id == sub_soccer_man[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_soccer_man[:sub_sport_id])
      elsif pt.team.event_id == sub_soccer_woman[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_soccer_woman[:sub_sport_id])
      elsif pt.team.event_id == sub_basketball_man[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_basketball_man[:sub_sport_id])
      elsif pt.team.event_id == sub_basketball_woman[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_basketball_woman[:sub_sport_id])
      elsif pt.team.event_id == sub_volley_inside_man[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_volley_inside_man[:sub_sport_id])
      elsif pt.team.event_id == sub_volley_inside_woman[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_volley_inside_woman[:sub_sport_id])
      elsif pt.team.event_id == sub_volley_outside_man[:event_id]
        PersonSubSportCategory.find_or_create_by(person: pt.person, sport_category_id: sub_volley_outside_man[:sub_sport_id])
      end
    end
  end
end
