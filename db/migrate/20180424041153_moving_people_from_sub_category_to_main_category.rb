class MovingPeopleFromSubCategoryToMainCategory < ActiveRecord::Migration[5.0]
  def change
    Person.joins(:sport_category).merge(SportCategory.only_children).all.each do |p|
      p.update(sport_category_id: p.sport_category.parent.id)
    end
  end
end
