class ChangeFullnameToFamilyNameOnPerson < ActiveRecord::Migration[5.0]
  def change
    rename_column :people, :fullname, :family_name
    rename_column :people, :givenname, :given_name
  end
end
