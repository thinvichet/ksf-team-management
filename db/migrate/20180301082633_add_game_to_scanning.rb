class AddGameToScanning < ActiveRecord::Migration[5.0]
  def change
    add_reference :scannings, :game, foreign_key: true
    add_column :scannings, :succeed, :boolean, default: false
    add_column :scannings, :message, :string, default: ''
  end
end
