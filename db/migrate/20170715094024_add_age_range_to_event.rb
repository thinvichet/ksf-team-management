class AddAgeRangeToEvent < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :age_from, :integer, default: 0
    add_column :events, :age_to, :integer, default: 100
  end
end
