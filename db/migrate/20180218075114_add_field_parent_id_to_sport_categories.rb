class AddFieldParentIdToSportCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :sport_categories, :parent_id, :integer
  end
end
