class AddCardTypeToPeople < ActiveRecord::Migration[5.0]
  def change
    create_table :card_types do |t|
      t.string :name
      t.string :letter
      t.boolean :allow_food, default: false

      t.timestamps
    end

    add_reference :people, :card_type, foreign_key: true

    # athlete   = CardType.create(name: 'Athlete', letter: 'F')
    # referee   = CardType.create(name: 'មន្រ្តីអាជ្ញាកណ្តាល', letter: 'Fo')
    # coach     = CardType.create(name: 'គ្រូបង្វឹក', letter: 'Fo')
    # medical   = CardType.create(name: 'ពេទ្យ', letter: 'Fo')
    # delegate  = CardType.create(name: 'មន្រ្តីរដ្ឋបាល', letter: 'Fo')

    # Person.all.each do |person|
    #   card_type =
    #     if person.athlete?
    #       athlete
    #     elsif person.referee?
    #       referee
    #     else
    #       role_in_event = person.position_in(Game.current_active.teams).first
    #       return nil if role_in_event.nil?
    #       if role_in_event == 'Coaches'
    #         coach
    #       elsif role_in_event == 'Medicals'
    #         medical
    #       elsif role_in_event == 'Delegates'
    #         delegate
    #       end
    #     end

    #   person.update(card_type: card_type)
    # end
  end
end
