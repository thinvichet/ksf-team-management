class AddSportCategoryAssociationToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :sport_category, foreign_key: true
  end
end
