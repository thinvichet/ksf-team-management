class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title
      t.text :location
      t.text :description
      t.date :start_date
      t.date :end_date
      t.string :title
      t.references :sport_category, foreign_key: true
      t.references :competition, foreign_key: true

      t.timestamps
    end
  end
end
