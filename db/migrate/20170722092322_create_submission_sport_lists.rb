class CreateSubmissionSportLists < ActiveRecord::Migration[5.0]
  def change
    create_table :submission_sport_lists do |t|
      t.references :sport_submission, foreign_key: true
      t.references :sport_category, foreign_key: true

      t.timestamps
    end
  end
end
