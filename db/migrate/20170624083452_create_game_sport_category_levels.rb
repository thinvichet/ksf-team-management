class CreateGameSportCategoryLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :game_sport_category_levels do |t|
      t.references :game_sport_category, foreign_key: true
      t.references :level, foreign_key: true

      t.timestamps
    end
  end
end
