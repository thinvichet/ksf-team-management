class AddFieldStatusToSportCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :sport_categories, :status, :string, default: ''
  end
end
