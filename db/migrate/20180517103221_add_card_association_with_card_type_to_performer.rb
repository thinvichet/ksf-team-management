class AddCardAssociationWithCardTypeToPerformer < ActiveRecord::Migration[5.0]
  def change
    add_reference :performers, :card_type, foreign_key: true
  end
end
