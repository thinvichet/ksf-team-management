class AddUniformNumberToPerson < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :uniform_number, :string, default: ''
  end
end
