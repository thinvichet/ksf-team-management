class AddAvatarAndDocumentsToAthletes < ActiveRecord::Migration[5.0]
  def change
    add_column :athletes, :avatar, :string
    add_column :athletes, :documents, :text
  end
end
