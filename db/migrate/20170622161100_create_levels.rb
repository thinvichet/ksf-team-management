class CreateLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :levels do |t|
      t.float :level_number, default: 1
      t.string :measurement_type, default: ''
      t.references :sport_category, foreign_key: true

      t.timestamps
    end
  end
end
