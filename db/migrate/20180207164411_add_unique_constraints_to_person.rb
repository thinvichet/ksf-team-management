class AddUniqueConstraintsToPerson < ActiveRecord::Migration[5.0]
  def change
    add_index :people, [:sure_name_kh, :given_name_kh, :date_of_birth, :phone, :identification_number], name: "index_people_on_name_kh_dob_phone_id", unique: true
  end
end
