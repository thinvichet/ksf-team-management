class AddPosititionToPersonTeams < ActiveRecord::Migration[5.0]
  def change
    add_column :person_teams, :position, :string, default: ''
  end
end
