class AddFieldOpenRegisteredAtAndEndRegisteredAt < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :open_registered_at, :date
    add_column :events, :close_registered_at, :date
  end
end
