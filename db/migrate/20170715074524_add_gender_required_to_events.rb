class AddGenderRequiredToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :gender_required, :string, default: 'both'
  end
end
