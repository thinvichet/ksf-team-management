class AddWeightRequiredToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :weight_required, :float, default: 0.0
  end
end
