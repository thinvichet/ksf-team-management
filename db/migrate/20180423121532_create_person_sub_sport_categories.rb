class CreatePersonSubSportCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :person_sub_sport_categories do |t|
      t.references :person, foreign_key: true
      t.references :sport_category, foreign_key: true

      t.timestamps
    end
  end
end
