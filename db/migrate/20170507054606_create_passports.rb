class CreatePassports < ActiveRecord::Migration[5.0]
  def change
    create_table :passports do |t|
      t.string :passport_picture, default: ''
      t.string :passport_number,  default: ''
      t.date :passport_validity
      t.string :nationality,      default: ''
      t.boolean :valid,           default: false
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
