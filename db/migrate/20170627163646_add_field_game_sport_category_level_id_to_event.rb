class AddFieldGameSportCategoryLevelIdToEvent < ActiveRecord::Migration[5.0]
  def change
    add_reference :events, :game_sport_category_level, foreign_key: true
  end
end
