class ChangeNameFieldOfPerson < ActiveRecord::Migration[5.0]
  def change
    rename_column :people, :family_name, :sure_name_kh
    rename_column :people, :given_name, :given_name_kh
    rename_column :people, :eng_family_name, :sure_name_en
    rename_column :people, :eng_given_name, :give_name_en
  end
end
