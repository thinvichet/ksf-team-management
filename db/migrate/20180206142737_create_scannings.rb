class CreateScannings < ActiveRecord::Migration[5.0]
  def change
    create_table :scannings do |t|
      t.references :person, foreign_key: true
      t.references :user, foreign_key: true
      t.string :scanning_type

      t.timestamps
    end
  end
end
