class AddLanguageFieldToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :local, :string
  end
end
