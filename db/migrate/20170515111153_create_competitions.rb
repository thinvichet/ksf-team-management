class CreateCompetitions < ActiveRecord::Migration[5.0]
  def change
    create_table :competitions do |t|
      t.string :name
      t.string :description
      t.references :sport_category, foreign_key: true

      t.timestamps
    end
  end
end
