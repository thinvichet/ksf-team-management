class AddFieldYearAndBatchToGames < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :year, :integer
    add_column :games, :batch, :integer

    Game.all.update_all(year: 2018, batch: 2)
  end
end
