class CreateGameSportCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :game_sport_categories do |t|
      t.references :game, foreign_key: true
      t.references :sport_category, foreign_key: true
      t.date :started_at

      t.timestamps
    end
  end
end
