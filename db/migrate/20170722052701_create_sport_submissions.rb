class CreateSportSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :sport_submissions do |t|
      t.integer :participant_amount
      t.references :game, foreign_key: true
      t.references :organization, foreign_key: true

      t.timestamps
    end
  end
end
