class ChangeLevelNumberDefaultValueOnLevels < ActiveRecord::Migration[5.0]
  def change
    change_column :levels, :level_number, :float, default: 0.0
  end
end
