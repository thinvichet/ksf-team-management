class CreatePerformers < ActiveRecord::Migration[5.0]
  def change
    create_table :performers do |t|
      t.string   "sure_name_kh"
      t.string   "given_name_kh"
      t.string   "sex"
      t.string   "email"
      t.string   "phone"
      t.string   "person_type"
      t.string   "avatar"
      t.datetime "created_at",                         null: false
      t.datetime "updated_at",                         null: false
      t.date     "date_of_birth"
      t.string   "sure_name_en"
      t.string   "given_name_en"
      t.string   "nationality"
      t.string   "citizenship"
      t.string   "address"
      t.string   "identification_number", default: ""
      t.integer  "user_id"

      t.index ["user_id"], name: "index_performers_on_user_id", using: :btree
      t.timestamps
    end
  end
end
