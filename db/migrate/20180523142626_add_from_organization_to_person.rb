class AddFromOrganizationToPerson < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :from_organization, :string, default: ""
  end
end
