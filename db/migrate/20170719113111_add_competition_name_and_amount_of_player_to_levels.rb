class AddCompetitionNameAndAmountOfPlayerToLevels < ActiveRecord::Migration[5.0]
  def change
    add_column :levels, :competition_name, :string
    add_column :levels, :number_of_player, :integer, default: 1
  end
end
