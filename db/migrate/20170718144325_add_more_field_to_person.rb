class AddMoreFieldToPerson < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :eng_family_name, :string
    add_column :people, :eng_given_name, :string
    add_column :people, :height, :float
    add_column :people, :weight, :float
    add_column :people, :blood_type, :string
    add_column :people, :nationality, :string
    add_column :people, :citizenship, :string
    add_column :people, :address, :string
  end
end
