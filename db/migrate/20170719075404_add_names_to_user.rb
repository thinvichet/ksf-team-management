class AddNamesToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sure_name_en, :string
    add_column :users, :given_name_en, :string
    add_column :users, :sure_name_kh, :string
    add_column :users, :given_name_kh, :string
  end
end
