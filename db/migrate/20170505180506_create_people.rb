class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :fullname
      t.string :givenname
      t.string :sex
      t.string :date_of_birth
      t.string :email
      t.string :phone
      t.string :person_type
      t.string :avatar
      t.references :team, foreign_key: true
      t.references :user, foreign_key: true
      t.references :sport_category, foreign_key: true

      t.timestamps
    end
  end
end
