class ChangeDateOfBirthColumnTypeToPeople < ActiveRecord::Migration[5.0]
  def change
    remove_column :people, :date_of_birth
    add_column :people, :date_of_birth, :date
  end
end
