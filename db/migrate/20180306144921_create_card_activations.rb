class CreateCardActivations < ActiveRecord::Migration[5.0]
  def change
    create_table :card_activations do |t|
      t.references :person, foreign_key: true
      t.references :game, foreign_key: true
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
