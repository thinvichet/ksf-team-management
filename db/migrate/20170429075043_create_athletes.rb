class CreateAthletes < ActiveRecord::Migration[5.0]
  def change
    create_table :athletes do |t|
      t.string :name
      t.string :phone
      t.string :nationality
      t.string :sex
      t.text :address
      t.date :date_of_birth
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
