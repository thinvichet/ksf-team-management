class RenameStartedDateAndEndDate < ActiveRecord::Migration[5.0]
  def change
    rename_column :events, :start_date, :started_at
    rename_column :events, :end_date, :ended_at
  end
end
