class CreateOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations do |t|
      t.string :org_label
      t.string :name
      t.string :short_code

      t.timestamps
    end
  end
end
