class ChangeGiveNameEnToGivenNameEnInPeople < ActiveRecord::Migration[5.0]
  def change
    rename_column :people, :give_name_en, :given_name_en
  end
end
