class RemoveColumnTeamIdFromPeople < ActiveRecord::Migration[5.0]
  def change
    remove_column :people, :team_id
  end
end
