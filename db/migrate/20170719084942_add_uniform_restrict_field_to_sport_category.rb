class AddUniformRestrictFieldToSportCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :sport_categories, :uniform_restrict, :bool, default: false
  end
end
