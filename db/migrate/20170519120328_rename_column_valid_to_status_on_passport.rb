class RenameColumnValidToStatusOnPassport < ActiveRecord::Migration[5.0]
  def change
    rename_column :passports, :valid, :status
  end
end
