class AddMeasurementToPersonTeams < ActiveRecord::Migration[5.0]
  def change
    add_column :person_teams, :measurement, :float, default: 0.0
  end
end
