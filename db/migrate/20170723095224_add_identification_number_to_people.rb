class AddIdentificationNumberToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :identification_number, :string, default: ''
  end
end
