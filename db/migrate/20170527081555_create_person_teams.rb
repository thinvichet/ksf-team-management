class CreatePersonTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :person_teams do |t|
      t.references :person, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
