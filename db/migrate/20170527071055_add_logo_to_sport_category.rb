class AddLogoToSportCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :sport_categories, :logo, :string
  end
end
