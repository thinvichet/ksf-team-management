class AddUniformsLabelsToSportCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :sport_categories, :uniform_labels, :string
    add_column :sport_categories, :uniform_amount, :integer, default: 2
  end
end
